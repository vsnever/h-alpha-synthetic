# -*- coding: utf-8 -*-
from __future__ import print_function
import numpy as np
from raysect.optical import World, translate, rotate
from raysect.optical.material import AbsorbingSurface
from raysect.optical.observer import PinholeCamera, RadiancePipeline2D, MonoAdaptiveSampler2D
from raysect.primitive import Cylinder, Subtract
from raysect.core.workflow import MulticoreEngine
from synthdiag import read_divimp_reg, divimp_fit_to_3d_wall, load_diag_data
from synthdiag.raysect import RegularRPhiZIntegrator, RegularRPhiZEmitter, load_pfc_mesh, FovCamera
from matplotlib import pyplot as plt


if __name__ == "__main__":
    case = 'o'
    diag = '55.E2'
    view = 'EP11_top'
    reflections = True
    detailed_wall = False
    dphi = dtheta = 0.05
    step = 0.0025
    cutoff = 0.1 if reflections else 0.05
    cutoff_frac = 0.01 if detailed_wall else 0.001
    min_samples = 1000 if reflections else 100
    nr = 460
    nz = 945
    rmin = 3.95
    rmax = 8.55
    zmin = -4.65
    zmax = 4.8
    wlmin = 655.6
    wlmax = 656.6
    eps_rz = 1.e-4
    roughness = {'Be': 0.26, 'W': 0.29, 'Ss': 0.13}
    view_param = load_diag_data()[diag][view]
    r, dr = np.linspace(rmin, rmax, nr + 1, retstep=True)
    z, dz = np.linspace(zmin, zmax, nz + 1, retstep=True)
    divimp_filename = 'i-ref-0003-90%s.mhf.h_alpha.txt' % case
    divimpprof = read_divimp_reg(divimp_filename, r, z, profcols={'Da': 9})
    emiss = divimp_fit_to_3d_wall(divimpprof['Da'], r, z) / (wlmax - wlmin)  # spectral emissivity
    world = World()
    central_column = Cylinder(rmin, zmax - zmin, material=AbsorbingSurface(), parent=world, transform=translate(0, 0, zmin))
    mesh = load_pfc_mesh(world, reflections=reflections, roughness=roughness, detailed=detailed_wall)
    emitt_material = RegularRPhiZEmitter(emiss, rmin, dr, dz, integrator=RegularRPhiZIntegrator(step))
    emitter = Subtract(Cylinder(rmax - eps_rz, zmax - zmin - eps_rz), Cylinder(rmin + eps_rz, zmax - zmin - eps_rz),
                       material=emitt_material, parent=world, transform=translate(0, 0, zmin))
    pipeline = RadiancePipeline2D(display_progress=False)
    # pipeline = RadiancePipeline2D(display_update_time=5.)
    sampler = MonoAdaptiveSampler2D(pipeline, fraction=1., cutoff=cutoff, min_samples=min_samples, ratio=10000.)
    if view == 'pinhole':
        camera = PinholeCamera((512, 512), fov=90., parent=world, transform=translate(8.3118, 1.4656, 0) * rotate(90., -10., -90.),
                               pipelines=[pipeline], frame_sampler=sampler)
    else:
        pixels = (int(round(view_param['fov'][0] / dphi)), int(round(view_param['fov'][1] / dtheta)))
        camera = FovCamera(pixels, fov=view_param['fov'], parent=world, pipelines=[pipeline], frame_sampler=sampler,
                           transform=translate(*view_param['pupil']) * rotate(*view_param['rotation']))
    camera.min_wavelength = wlmin
    camera.max_wavelength = wlmax
    camera.spectral_bins = 1
    camera.spectral_rays = 1
    camera.pixel_samples = min_samples
    camera.ray_extinction_prob = 0
    camera.render_engine = MulticoreEngine(24)
    imsize = camera.pixels[0] * camera.pixels[1]
    npix = imsize
    fname = "Dalpha_%s_%s_case_%s" % (diag, view, case)
    # start ray tracing
    # plt.ion()
    while npix > cutoff_frac * imsize:
        camera.observe()
        errors = pipeline.frame.errors()
        ind = np.where(pipeline.frame.mean > 1.e-4 * pipeline.frame.mean.max())
        rel_err = errors[ind] / pipeline.frame.mean[ind]
        npix = np.sum(rel_err > cutoff)
        print("Max. error: %.2e. Pixels to add samples for: %d" % (rel_err.max(), npix))
        np.savetxt(fname + '.txt', pipeline.frame.mean)
        np.savetxt(fname + '_errors.txt', pipeline.frame.errors())
    # plt.ioff()
