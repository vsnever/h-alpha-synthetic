# -*- coding: utf-8 -*-
from __future__ import print_function
import os
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.patches import Rectangle
from synthdiag.raysect import FovCamera
from synthdiag import load_diag_data


if __name__ == "__main__":
    case = 'o'
    diag = '55.E2'
    view = 'EP11_top'
    if not os.path.isdir('rtm_gif'):
        os.makedirs('rtm_gif')
    fov_file = "Dalpha_%s_case_%s.txt" % (view, case)
    fov_data = np.loadtxt(fov_file)  # divimp_example.py output
    view_param = load_diag_data()[diag][view]
    camera = FovCamera(fov_data.shape, fov=view_param['fov'])
    rtm = np.load('RTM_%s.npy' % view)[0]  # RTM for the 1st (and only) pixel in x direction
    rtmsum = rtm.sum((1, 2))
    npix_y = rtm.shape[0]
    dtheta = 2 * camera.theta[0] / npix_y  # in FovCamera theta goes from positive to negative values
    nr = rtm.shape[1]
    nz = rtm.shape[2]
    r = np.linspace(3.95, 8.55, nr + 1)
    z = np.linspace(-4.65, 4.8, nz + 1)
    fig = plt.figure(1, figsize=(6., 7.))
    ax_rtm = fig.add_axes([0.09, 0.3, 0.39, 0.66])  # axes for RTM
    ax_cb = fig.add_axes([0.49, 0.3, 0.025, 0.66])  # axes for RTM's colorbar
    ax_view = fig.add_axes([0.71, 0.3, 0.25, 0.66])  # axes for the radiance in the FoV
    ax_unit = fig.add_axes([0.09, 0.08, 0.85, 0.15])  # axes for the RTM's sum (unit emissivity)
    n999 = int(0.999 * fov_data.size)
    vmax = np.sort(fov_data.flatten())[n999]  # just to filter out some bad pixels if any
    ax_view.pcolormesh(camera.phi, camera.theta, fov_data.T, vmax=vmax, cmap='gray')
    ax_view.axvline(-0.3, color='r', lw=0.75)
    ax_view.axvline(0.3, color='r', lw=0.75)
    ax_view.set_aspect('equal')
    rect = Rectangle((-0.3, camera.theta[0] - dtheta), 0.6, dtheta, facecolor='r', edgecolor='r')
    ax_view.add_artist(rect)
    ax_view.set_xlabel(r'$\phi$, $^o$')
    ax_view.set_ylabel(r'$\theta$, $^o$')
    ax_view.set_title('%s FoV (case %s)' % (view.replace('_', ' '), case))
    theta_rtm = np.linspace(camera.theta[0] - 0.5 * dtheta, camera.theta[-1] + 0.5 * dtheta, rtmsum.size)
    ax_unit.plot(theta_rtm, rtmsum)
    point, = ax_unit.plot(theta_rtm[0], rtmsum[0], 'x', color='r')
    ax_unit.invert_xaxis()
    ax_unit.spines['right'].set_color('none')
    ax_unit.spines['top'].set_color('none')
    ax_unit.set_xlabel(r'$\theta$, $^o$')
    ax_unit.set_ylabel('Sum(RTM), m')
    for i in range(npix_y):
        ax_rtm.cla()
        ax_cb.cla()
        rtm_i = rtm[i].T
        rtm_i[rtm_i < 1.e-16] = 1.e-16
        rtm_log = np.log10(rtm[i].T)
        vmax = rtm_log.max()
        pcm = ax_rtm.pcolormesh(r, z, rtm_log, vmax=vmax, vmin=vmax - 5., cmap='gnuplot')
        ax_rtm.set_aspect('equal')
        ax_rtm.set_title(r'RTM at $\theta$ = %.2f $^o$' % theta_rtm[i])
        ax_rtm.set_xlabel('R, m')
        ax_rtm.set_ylabel('Z, m')
        cb = fig.colorbar(pcm, cax=ax_cb)
        ax_cb.set_ylabel('log$_{10}$(m)')
        point.set_ydata([rtmsum[i]])
        point.set_xdata([theta_rtm[i]])
        rect.set_y(camera.theta[0] - i * dtheta)
        fig.savefig('rtm_gif/RTM_%s_%03d.png' % (view, i))
# change default ImageMagick memory policy in /etc/ImageMagick-*/policy.xml if required
os.system("convert -delay 10 -loop 0 rtm_gif/RTM_%s_*.png RTM_%s.gif" % (view, view))
# or use GraphicsMagick instead
# os.system("gm convert -delay 10 -loop 0 rtm_gif/RTM_%s_*.png RTM_%s.gif" % (view, view))
