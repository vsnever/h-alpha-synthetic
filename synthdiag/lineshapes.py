# -*- coding: utf-8 -*-
# Created by Vladislav Neverov (NRC "Kurchatov Institute") to use with Raysect and CHERAB Spectroscopy Modelling Framework
from __future__ import print_function
import os
import numpy as np
import scipy.constants as const


mass_dict = {'H': 938890000.0, 'D': 1876123778.3, 'T': 2809431890.0, 'B': 1.007e10, 'Be': 8.394795e9, 'C': 1.1188e10,
             'Ca': 3.7332e10, 'He': 3.7284e9, 'Kr': 7.8057e10, 'Mg': 2.264e10, 'Na': 2.141483e10, 'Ne': 1.87973e10,
             'O': 1.4903e10, 'Si': 2.6161e10}  # element/isotope mass in eV


def gauss_ls(mass, wavelength, tion, wlcenters, intensities):
    """ Multi-gaussian 2D (space, wavelength) line shape profile
        Parameters
        ----------
        mass: float
            Isotope mass in eV
        wavelengths: numpy.ndarray
            Wavelength array (nm)
        tion: numpy.ndarray
            Ion temperature profile (eV)
        wlcenters: 2D numpy.ndarray
            Spatial profile of the centers of single gaussian components (nm)
        intensities: 2D numpy.ndarray
            Spatial profile of the intensities of single gaussian components
    """
    c = 0.5 * mass / tion
    a = intensities / wlcenters
    f = 0
    for i in range(intensities.shape[1]):
        f += a[:, i, None] * np.exp(-c[:, None] * (wavelength[None, :] / wlcenters[:, i, None] - 1.)**2)

    return np.sqrt(c / np.pi)[:, None] * f


class AdasLineShape():
    """ Doppler-Zeeman spectral line shape with the fine structure taken from ADAS (ADAS603)"""

    def __init__(self, path=os.path.join(os.path.dirname(os.path.realpath(__file__)), 'adas/data/adas603'),
                 mass=None, element='Be', line='527nm'):
        """ Parameters
            ----------
            path: string, optional
                Path to the profiles extracted from ADAS:
                Wavelength(Bmag), Intensity_parallel(Bmag), Intensity_perpendicular(Bmag)
            mass: float, optional
                Element/isotope mass in eV
            element: string, optional
                Element/isotope symbol. Default: 'Be'.
            line: string, optional
                Spectral line. Default: '527nm'.
        """
        self.mass = mass or mass_dict[element]
        self.line = line
        self.element = element
        data = np.loadtxt(os.path.join(path, '%s_%s_0_intensities.txt' % (element, line)))
        self.bmag = data[:, 0]
        self.bstep = self.bmag[1] - self.bmag[0]
        self.intensities = [data[:, 1:], np.loadtxt(os.path.join(path, '%s_%s_90_intensities.txt' % (element, line)))[:, 1:]]
        self.wlcenters = np.loadtxt(os.path.join(path, '%s_%s_wavelengths.txt' % (element, line)))[:, 1:]

    def __call__(self, wavelength, tion, bmag, costheta):
        """ Returns spectral emission profile with respect to magnetic field's direction.
            Parameters
            ----------
            wavelengths: numpy.ndarray
                Wavelength array
            tion: numpy.ndarray
                Temperature spatial profile
            bmag: numpy.ndarray
                Magnetic field strength spatial profile
            costheta: numpy.ndarray
                Cosine of the angle between the magnetic field direction and the line of sight
        """
        indxb = (bmag // self.bstep).astype(int)
        weight = (bmag % self.bstep) / self.bstep
        intens = [(1. - weight)[:, None] * int_c[indxb] + weight[:, None] * int_c[indxb + 1] for int_c in self.intensities]
        wlcenters = (1. - weight)[:, None] * self.wlcenters[indxb] + weight[:, None] * self.wlcenters[indxb + 1]
        costheta2 = costheta * costheta
        intensities = intens[0] * costheta2[:, None] + intens[1] * (1. - costheta2)[:, None]
        intensities /= intensities.sum(1)[:, None]

        return gauss_ls(self.mass, wavelength, tion, wlcenters, intensities)

    def component(self, wavelength, tion, bmag, parallel=True):
        """ Returns spectral emission profile parallel or perpendicular to magnetic field's direction.
            Parameters
            ----------
            wavelengths: numpy.ndarray
                Wavelength array
            tion: numpy.ndarray
                Temperature spatial profile
            bmag: numpy.ndarray
                Magnetic field strength spatial profile
            parallel: bool, optional
                True for the parallel component and False for the perpendicular component
        """
        i = 0 if parallel else 1
        # interpolate over available data from adas603
        indxb = (bmag // self.bstep).astype(int)
        weight = (bmag % self.bstep) / self.bstep
        intensities = (1. - weight)[:, None] * self.intensities[i][indxb] + weight[:, None] * self.intensities[i][indxb + 1]
        intensities /= intensities.sum(1)[:, None]
        wlcenters = (1. - weight)[:, None] * self.wlcenters[indxb] + weight[:, None] * self.wlcenters[indxb + 1]

        return gauss_ls(self.mass, wavelength, tion, wlcenters, intensities)


class SimpleLineShape():
    """ Simple Doppler-Zeeman triplet (in the case of Paschen-Back effect)"""

    def __init__(self, mass=None, element='Be', linecenter=457.27):
        """ Parameters
            ----------
            mass: float, optional
                Element/isotope mass in eV
            element: string, optional
                Element/isotope symbol. Default: 'Be'.
            linecenter: float, optional
                Spectral line center in nm. Default: 457.27.
        """
        self.mass = mass or mass_dict[element]
        self.linecenter = linecenter
        self.mbohr = const.value('Bohr magneton in eV/T')
        c = const.c  # m/s
        h = const.value('Planck constant in eV s')
        self.hc = h * c * 1.e+9
        self.omega0 = self.hc / self.linecenter

    def __call__(self, wavelength, tion, bmag, costheta):
        """ Returns spectral emission profile with respect to magnetic field's direction.
            Parameters
            ----------
            wavelengths: numpy.ndarray
                Wavelength array
            tion: numpy.ndarray
                Temperature spatial profile
            bmag: numpy.ndarray
                Magnetic field strength spatial profile
            costheta: numpy.ndarray
                Cosine of the angle between the magnetic field direction and the line of sight
        """
        omega_b = self.mbohr * bmag
        wlcenters = np.zeros((bmag.size, 3))
        wlcenters[:, 0] = self.linecenter
        wlcenters[:, 1] = self.hc / (self.omega0 + omega_b)
        wlcenters[:, 2] = self.hc / (self.omega0 - omega_b)
        intensities = np.zeros((bmag.size, 3))
        costheta2 = costheta * costheta
        sintheta2 = 1. - costheta2
        intensities[:, 0] = 0.5 * sintheta2
        intensities[:, 1] = 0.25 * sintheta2 + 0.5 * costheta2
        intensities[:, 2] = intensities[:, 1]

        return gauss_ls(self.mass, wavelength, tion, wlcenters, intensities)

    def component(self, wavelength, tion, bmag, parallel=True):
        """ Returns spectral emission profile parallel or perpendicular to magnetic field's direction.
            Parameters
            ----------
            wavelengths: numpy.ndarray
                Wavelength array
            tion: numpy.ndarray
                Temperature spatial profile
            bmag: numpy.ndarray
                Magnetic field strength spatial profile
            parallel: bool, optional
                True for the parallel component and False for the perpendicular component
        """
        return self.__call__(wavelength, tion, bmag, 1.) if parallel else self.__call__(wavelength, tion, bmag, 0)


if __name__ == "__main__":
    pass
