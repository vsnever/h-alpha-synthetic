# -*- coding: utf-8 -*-
from __future__ import print_function
import numpy as np


class FoV():
    """ Diagnostic field of view"""

    VIEWS = ['EP11_top', 'EP11_bottom', 'EP12', 'DIM_UP01', 'DIM_EP01', 'DIM_LP02U', 'DIM_LP02L', 'DIM_LP02I', 'DIM_LP02O']
    PUPILS = {'EP11_top': np.array([-7.8865, -4.5533, 0.0652]),
              'EP11_bottom': np.array([-7.8865, -4.5533, 1.139]),
              'EP12': np.array([-6.0131, -6.4207, 1.3484]),
              'DIM_UP01': np.array([6.58958, 1.06069, 4.22578]),
              'DIM_EP01': np.array([8.72735, 0.94514, 1.30836]),
              'DIM_LP02U': np.array([7.41925, 3.72981, -3.72056]),
              'DIM_LP02L': np.array([5.57309, 2.79904, -4.66300]),
              'DIM_LP02I': np.array([4.097, 2.383, -3.990]),
              'DIM_LP02O': np.array([4.379, 2.508, -4.118])}
    CORNERS = {'EP11_top': {'tl': np.array([-5.3997, -2.2652, 4.4335]), 'tr': np.array([-4.7664, -3.0621, 4.5353]),
                            'bl': np.array([-3.8984, -1.2698, 1.4013]), 'br': np.array([-3.4922, -2.1481, 1.3608])},
               'EP11_bottom': {'tl': np.array([-3.8916, -1.2905, 1.7867]), 'tr': np.array([-3.5016, -2.1328, 1.7680]),
                               'bl': np.array([-3.9563, -1.2108, -2.5632]), 'br': np.array([-3.4486, -2.2175, -2.4598])},
               'EP12': {'tl': np.array([-4.6498, 3.8601, 4.2905]), 'tr': np.array([-4.0676, -0.5926, 3.0928]),
                        'bl': np.array([-4.5509, 4.7992, -2.7736]), 'br': np.array([-4.0557, -0.6010, -0.8900])},
               'DIM_UP01': {'tl': np.array([4.09192, 0.52303, -2.55727]), 'tr': np.array([4.04608, 0.76889, -2.55652]),
                            'bl': np.array([6.19457, 0.83104, -3.23730]), 'br': np.array([6.15517, 1.08264, -3.23292])},
               'DIM_EP01': {'tl': np.array([2.98903, 3.10386, -2.64537]), 'tr': np.array([2.74719, 3.27682, -2.62115]),
                            'bl': np.array([3.71726, 4.50994, -3.34125]), 'br': np.array([3.49701, 4.72938, -3.32783])},
               'DIM_LP02U': {'tl': np.array([3.64850, 1.82540, -2.42614]), 'tr': np.array([3.63593, 1.84574, -2.42562]),
                             'bl': np.array([3.36309, 1.67128, -4.27696]), 'br': np.array([3.35276, 1.69205, -4.27696])},
               'DIM_LP02L': {'tl': np.array([3.64424, 1.82128, -2.45029]), 'tr': np.array([3.63367, 1.83899, -2.44861]),
                             'bl': np.array([3.23042, 1.61517, -3.90609]), 'br': np.array([3.22083, 1.63409, -3.90588])},
               'DIM_LP02I': {'tl': np.array([3.87692, 2.15405, -3.33547]), 'tr': np.array([3.78911, 2.29566, -3.36182]),
                             'bl': np.array([3.65699, 2.05535, -3.84669]), 'br': np.array([3.58703, 2.18442, -3.83413])},
               'DIM_LP02O': {'tl': np.array([4.78153, 2.83086, -3.88286]), 'tr': np.array([4.87012, 2.67867, -3.90103]),
                             'bl': np.array([4.77058, 2.85019, -4.49158]), 'br': np.array([4.86231, 2.69284, -4.47999])}}

    def __init__(self, view='EP11_top', pupil=np.array([8.9682, 1.5813, 0.5]),
                 direction=np.array([-0.98481, -0.17365, 0]), fov_angles=(90., 70.),
                 phi_step=0.025, theta_step=0.025):
        """ Selects rays for a given line of sight (LoS). Returns ray's indeces.
            Parameters
            ----------
            view: string, optional
                ITER H-alpha spectroscopy view: 'EP11_top' (default), 'EP11_bottom' or 'EP12'.
            pupil: numpy.ndarray (size=3), optional
                Pupil location for non-H-alpha FoV
            direction: numpy.ndarray (size=3), optional
                View direction for non-H-alpha FoV
            fov_angles: 2-tuple, optional
                FoV angles: (azimuthal (horizontal), polar (vertical)) for non-H-alpha FoV
            phi_step: float, optional
                Azimuthal (horizontal) angle grid step in degrees. Default: 0.025.
            theta_step: float, optional
                Polar (vertical) angle grid step in degrees. Default: 0.025.
        """
        self.view = view
        self.pupil = self.PUPILS[view] if view in self.VIEWS else pupil
        self.cs, self.theta_max, self.phi_max = self.calc_cs_view() if view in self.VIEWS else self.calc_cs(direction, fov_angles)
        self.rays, self.theta, self.phi = self.create_rays(phi_step, theta_step)

    def calc_cs_view(self):
        if self.view not in self.VIEWS:
            raise ValueError('Only the following views are supported %s' % (' '.join(self.VIEWS)))
        ray_corners = {}
        for key, value in self.CORNERS[self.view].items():
            ray = value - self.pupil
            ray /= np.sqrt(ray.dot(ray))
            ray_corners[key] = ray
        cs = np.zeros((3, 3))
        cs[2, :] = sum(ray_corners.values())
        cs[2, :] /= np.sqrt(cs[2, :].dot(cs[2, :]))
        cs[0, :] = ray_corners['tl'] - ray_corners['tr']
        cs[0, :] /= np.sqrt(cs[0, :].dot(cs[0, :]))
        cs[1, :] = np.cross(cs[2, :], cs[0, :])
        cs[1, :] /= np.sqrt(cs[1, :].dot(cs[1, :]))
        phi_max = 0.5 * np.arccos(ray_corners['tl'].dot(ray_corners['tr']))
        theta_max = 0.5 * np.arccos(ray_corners['tl'].dot(ray_corners['bl']))

        return cs, theta_max, phi_max

    def calc_cs(self, direction, fov_angles):
        cs = np.zeros((3, 3))
        cs[2, :] = direction / np.sqrt(direction.dot(direction))
        a = np.sqrt(cs[2, 1] * cs[2, 1] + cs[2, 0] * cs[2, 0])
        cs[0, 0] = -cs[2, 1] / a
        cs[0, 1] = cs[2, 0] / a
        cs[1, :] = np.cross(cs[2, :], cs[0, :])
        cs[1, :] /= np.sqrt(cs[1, :].dot(cs[1, :]))
        theta_max = np.pi * fov_angles[1] / 360.
        phi_max = np.pi * fov_angles[0] / 360.

        return cs, theta_max, phi_max

    def euler(self):
        """ Returns (yaw, pitch, roll) for (-Y)*(-X)'Z'' intrinsic rotation"""
        rad2deg = 180. / np.pi
        pitch = np.arcsin(self.cs[2, 1]) * rad2deg
        yaw = np.arctan2(-self.cs[2, 0], self.cs[2, 2]) * rad2deg
        roll = np.arctan2(self.cs[0, 1], self.cs[1, 1]) * rad2deg

        return (yaw, pitch, roll)

    def create_rays(self, phi_step=0.025, theta_step=0.025):
        nphi = int(360. * self.phi_max / (phi_step * np.pi)) + 1
        ntheta = int(360. * self.theta_max / (theta_step * np.pi)) + 1
        phi = np.linspace(-self.phi_max, self.phi_max, nphi)
        theta = np.linspace(-self.theta_max, self.theta_max, ntheta)
        phic = 0.5 * (phi[1:] + phi[:-1])
        thetac = 0.5 * (theta[1:] + theta[:-1])
        rays = (self.cs[2][None, None, :] * (np.cos(phic[None, :, None]) * np.cos(thetac[:, None, None])) -
                self.cs[0][None, None, :] * (np.sin(phic[None, :, None]) * np.cos(thetac[:, None, None])) +
                self.cs[1][None, None, :] * np.sin(thetac[:, None, None]))
        rays /= np.sqrt((rays * rays).sum(2)[:, :, None])

        return rays, theta, phi

    def select_rays_los(self, angle_h, angle_v, angle_cone=1.2):
        """ Selects rays for a given line of sight (LoS). Returns ray's indeces.
            Parameters
            ----------
            angle_h: float
                Azimutal angle of LoS center in FoV coordinates
            angle_v: float
                Polar angle of LoS center in FoV coordinates
            angle_cone: float, optional
                LoS cone angle in degrees. Default: 1.2
        """
        angle_h_rad = angle_h * np.pi / 180.
        angle_v_rad = angle_v * np.pi / 180.
        half_angle_cone_rad = np.pi * angle_cone / 360.
        thetac = 0.5 * (self.theta[1:] + self.theta[:-1])
        phic = 0.5 * (self.phi[1:] + self.phi[:-1])
        theta2d, phi2d = np.meshgrid(thetac, phic, indexing='ij')
        indx = np.where((theta2d - angle_v_rad)**2 + (phi2d - angle_h_rad)**2 < half_angle_cone_rad**2)

        return indx


if __name__ == "__main__":
    for view in ["DIM_EP01", "DIM_UP01", "DIM_LP02U", "DIM_LP02L", "DIM_LP02I", "DIM_LP02O"]:
        fov = FoV(view=view)
        print(view)
        print('"pupil": [%.4f, %.4f, %.4f]' % (fov.pupil[0], fov.pupil[1], fov.pupil[2]))
        print('"rotation": [%.4f, %.4f, %.4f]' % fov.euler())
        print('"fov": [%.4f, %.4f]' % (360. * fov.phi_max / np.pi, 360. * fov.theta_max / np.pi))
