# -*- coding: utf-8 -*-
from __future__ import print_function
from glob import glob
import os
import json


def load_diag_data(path=os.path.join(os.path.dirname(__file__), 'ITER_diagnostics'), diagnostics=None):
    if diagnostics is not None:
        files = [os.path.join(path, '%s.json' % diag) for diag in diagnostics]
    else:
        files = glob(os.path.join(path, '*.json'))
    diag_data = {}
    for f in files:
        with open(f, 'r') as jsonfile:
            diag_data[os.path.basename(f)[:-5]] = json.load(jsonfile)

    return diag_data


if __name__ == "__main__":
    pass
