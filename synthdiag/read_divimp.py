# -*- coding: utf-8 -*-
# Created by Vladislav Neverov (NRC "Kurchatov Institute") to use with Raysect and CHERAB Spectroscopy Modelling Framework
from __future__ import print_function
import numpy as np
from scipy.interpolate import LinearNDInterpolator, interp1d
from scipy.spatial import Delaunay


class TriMask:
    "Masks redundant triangles in Delaunay triangulation of DIVIMP mesh"

    def __init__(self, x, y, simplices):
        """ Parameters
            ----------
            x, y : 1d numpy.ndarray
                Coordinates of verticies.
            simplices : 2d numpy.ndarray of ints, shape (N, 3)
                Indices of the points forming the simplices in the triangulation
        """
        self.xv = x[simplices].T
        self.yv = y[simplices].T

    def calc_feature(self, feature='perimeter'):
        if feature == 'area':
            return self.calc_area()
        edgelengths = self.calc_edgelengths()
        if feature == 'perimeter':
            return edgelengths.sum(0)
        if feature == 'longedge':
            return edgelengths.max(0)

    def calc_area(self):
        return np.abs((self.xv[0] - self.xv[1]) * (self.yv[2] - self.yv[1]) -
                      (self.yv[0] - self.yv[1]) * (self.xv[2] - self.xv[1])) * 0.5

    def calc_edgelengths(self):
        edgelengths = np.zeros(self.xv.shape)
        ydiff = self.yv[0] - self.yv[1]
        xdiff = self.xv[0] - self.xv[1]
        edgelengths[0] = np.sqrt(xdiff * xdiff + ydiff * ydiff)
        ydiff = self.yv[0] - self.yv[2]
        xdiff = self.xv[0] - self.xv[2]
        edgelengths[2] = np.sqrt(xdiff * xdiff + ydiff * ydiff)
        ydiff = self.yv[2] - self.yv[1]
        xdiff = self.xv[2] - self.xv[1]
        edgelengths[1] = np.sqrt(xdiff * xdiff + ydiff * ydiff)

        return edgelengths

    def __call__(self, feature='perimeter', factor=3.5, factor_div=1.5):
        """ Creates the mask for redundant triangles.
            All triangles are compared by their features (perimeter, area or longest edge) with the average value.
            Triangles with 'feature > factor * average_feature' in the main chamber
            and 'feature > factor_div * average_feature' in the divertor are considered as redundant.

                Parameters
                ----------
                feature : string ('perimeter', 'area' or 'longedge')
                    Compare the triangles by their perimeters, areas or longest edges
                    Default: 'perimeter'
                factor : float, optional
                    Factor for the main chamber
                    Default: 3.5
                factor_div : float, optional
                    Factor for the divertor
                    Default: 1.5
        """
        zdiv = -3.7
        rdiv = 5.3
        feature_val = self.calc_feature(feature='perimeter')
        mean_feature = feature_val.mean()
        div_simplices = (self.yv.max(0) < zdiv) * (self.xv.max(0) < rdiv)
        mask = (feature_val > factor * mean_feature) + ((feature_val > factor_div * mean_feature) * div_simplices)

        return mask


def grind_mesh(ln, r, z, profiles, factor=8, kind='linear'):
    """ Grinds original divimp mesh along the grid lines

            Parameters
            ----------
            ln : numpy.ndarray
                Grid line numbers
            r, z : numpy.ndarray
                R, Z coordinates
            profiles : dict of numpy.ndarray
                Plasma profiles
            factor : int, optional
                Grinding factor. For factor=X, the final mesh is X times denser.
                Default: 8
            kind : string, optional
                Kind of interpolation ('linear', 'cubic', etc.) to use along the grid lines for the profiles. See interp1d() manual.
                Default: 'linear'

            Returns
            -------
            (rnew, znew, profiles_new)
            rnew, znew : numpy.ndarray
                Refined R and Z coordinates
            profiles_new : dict of numpy.ndarray
                Plasma profiles on the refined mesh
    """
    rnew = np.zeros(factor * (r.size - 1) + 1)
    znew = np.zeros(factor * (r.size - 1) + 1)
    profiles_new = {key: np.zeros(factor * (r.size - 1) + 1) for key in profiles.keys()}
    for i in range(ln[0], ln[-1] + 1):
        points, = np.where(ln == i)
        n = points.size
        if not n:
            continue
        kind1 = 'linear' if n < 4 else kind
        ibegin = points[0]
        iend = points[-1] + 1
        p = np.linspace(0, n - 1, n)
        nnew = factor * (n - 1) + 1
        pnew = np.linspace(0, n - 1, nnew)
        ri = interp1d(p, r[ibegin:iend], kind='linear')
        zi = interp1d(p, z[ibegin:iend], kind='linear')
        rnew[factor * ibegin:factor * (iend - 1) + 1] = ri(pnew)
        znew[factor * ibegin:factor * (iend - 1) + 1] = zi(pnew)
        for key in profiles.keys():
            val = interp1d(p, profiles[key][ibegin:iend], kind=kind1)
            profiles_new[key][factor * ibegin:factor * (iend - 1) + 1] = val(pnew)

    return rnew, znew, profiles_new


def read_divimp_reg(filename, r, z, profcols={'0': 8, '1': 9}, interp_kind='linear', **kwargs):
    """ Reads divimp mesh and interpoales it on a regular RZ grid

            Parameters
            ----------
            filename : numpy.ndarray
                Grid line numbers
            r, z : numpy.ndarray
                R, Z coordinates of the cell borders of regular grid
            profcols : dict
                Plasma profile names and respective columns in divimp file
            interp_kind : string, optional
                Kind of interpolation
            **kwargs
                numpy.loadtxt() kwargs

            Returns
            -------
            profiles2d : dict of numpy.ndarray
                2D profiles on the regular grid (same keys as in profcols)
    """
    data = np.loadtxt(filename, **kwargs)
    ln = data[:, 1].astype(int)
    r0 = data[:, 2]
    z0 = data[:, 3]
    profiles0 = {key: data[:, val] for key, val in profcols.items()}
    r1, z1, profiles = grind_mesh(ln, r0, z0, profiles0, factor=8, kind=interp_kind)
    tri = Delaunay(np.array([r1, z1]).T)
    mask = TriMask(r1, z1, tri.simplices)(feature='perimeter')
    bad_simpl_indx, = np.where(mask)
    rc = 0.5 * (r[1:] + r[:-1])
    zc = 0.5 * (z[1:] + z[:-1])
    r2d, z2d = np.meshgrid(rc, zc)
    points_new = np.array([r2d.flatten(), z2d.flatten()]).T
    simpl_indx = tri.find_simplex(points_new)
    mask_data = np.in1d(simpl_indx, bad_simpl_indx)
    profiles2d = {}
    for key, val in profiles.items():
        prof = LinearNDInterpolator(tri, profiles[key], fill_value=0)(points_new)
        prof[mask_data] = 0
        profiles2d[key] = prof.reshape(r2d.shape).T

    return profiles2d


def divimp_fit_to_3d_wall(data, r, z, zminr=-3.39, zminl=-3.93,
                          zmaxr=(-2.54, -2.51, -2.49, -2.46, -2.42, 2.84, 2.96, 3.77, 3.82, 3.89, 3.96, 4.06, 4.10, 4.13, 4.19),
                          zmaxl=(-3.87, -3.62, -3.34, -3.24, -3.14, -3.04, -3.02), rmax=4.8, rmin=5.35,
                          rshift=(0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.07, 0.06, 0.05, 0.04, 0.03, 0.02, 0.01),
                          lshift=(0.01, 0.02, 0.03, 0.04, 0.03, 0.02, 0.01)):
    """ Use this function only for the data obtained on the extended 2D grid (OSM + DIVIMP)."""
    iz0 = np.argmax(z > zminr)
    ir = np.argmax(r > rmax)
    for i in range(len(zmaxr)):
        iz1 = np.argmax(z > zmaxr[i])
        if iz1 > iz0:
            n = int(round(rshift[i] / (r[1] - r[0])))
            data[n:n + ir, iz0:iz1] = data[:ir, iz0:iz1]
            data[:n, iz0:iz1] = 0
            iz0 = iz1
    iz0 = np.argmax(z > zminl)
    ir = np.argmax(r > rmin)
    for i in range(len(zmaxl)):
        iz1 = np.argmax(z > zmaxl[i])
        if iz1 > iz0:
            n = int(round(lshift[i] / (r[1] - r[0])))
            data[ir:-n, iz0:iz1] = data[ir + n:, iz0:iz1]
            data[-n:, iz0:iz1] = 0
            iz0 = iz1

    return data


if __name__ == "__main__":
    pass
