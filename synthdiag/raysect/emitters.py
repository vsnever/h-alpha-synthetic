# -*- coding: utf-8 -*-
# Created by Vladislav Neverov (NRC "Kurchatov Institute") to use with Raysect and CHERAB Spectroscopy Modelling Framework
import warnings
import numpy as np
from raysect.optical.material import VolumeIntegrator, InhomogeneousVolumeEmitter


class RegularRPhiZIntegrator(VolumeIntegrator):
    """ Integrates the emission along ray's trajectory. Use with RegularRPhiZEmitter and DiscreteSpectralRPhiZEmitter material."""

    def __init__(self, step=0.005):
        """ Parameters
            ----------
            step : float, optional
                Integration step (in meters). Default: 0.005.
        """
        self.step = step
        self.rad2deg = 180. / np.pi

    def integrate(self, spectrum, world, ray, primitive, material, start_point, end_point, world_to_primitive, primitive_to_world):
        start = start_point.transform(world_to_primitive)  # start point in local coordinates
        end = end_point.transform(world_to_primitive)  # end point in local coordinates
        direction = start.vector_to(end)  # direction of integration
        length = direction.length  # integration length
        if length < 0.1 * self.step:  # return if ray's path is too short
            return spectrum
        direction = direction.normalise()  # normalized direction
        n = max(2, int(length / self.step))  # number of points along ray's trajectory
        t, dt = np.linspace(0, length, n, retstep=True)  # regulary scattered points along ray's trajectory and integration step
        t = t[:-1] + 0.5 * dt  # moving them into the centers of the intervals (and removing the last point)
        x = start.x + direction.x * t  # x coordinates of the points
        y = start.y + direction.y * t  # y coordinates of the points
        z = start.z + direction.z * t  # z coordinates of the points
        iz = (z / material.dz).astype(int)  # Z-indices of grid cells, in which the points are located
        r = np.sqrt(x * x + y * y)  # R coordinates of the points
        ir = ((r - material.rmin) / material.dr).astype(int)  # R-indices of grid cell, in which the points are located
        # the emitter is located in the bounding cylinders such a way that we always have:
        # 0 <= ir < emiss.shape[0] and 0 <= iz < emiss.shape[-1], so the boundary check is not requred
        if material.is3d:  # material is a 3D emitter
            phi = self.rad2deg * np.arctan2(y, x)  # phi coordinates of the points (in degrees)
            phi[phi < 0] += 360.  # making them all in [0, 360) interval
            phi = phi % material.phimax  # moving into the [0, phimax) sector
            iphi = (phi / material.dphi).astype(int)  # phi-indices of grid cell, in which the points are located
            emiss = material.emiss[ir, iphi, iz]  # emission profile along ray's trajectory
        else:  # material is a 2D emitter
            emiss = material.emiss[ir, iz]  # emission profile along ray's trajectory
        spectrum.samples[material.indx] += emiss.sum(0) * dt  # volume integral along ray's trajectory

        return spectrum


class ZeemanRegularRPhiZIntegrator(VolumeIntegrator):
    """ Integrates spectral emission along ray's trajectory in RegularRPhiZEmitter material
        with respect to magnetic field direction"""

    def __init__(self, step=0.005, threshold=1.e-5):
        """ Parameters
            ----------
            step : float, optional
                Integration step (in meters). Default: 0.005.
            threshold : float, optional
                Zero threshold for the emission. Default: 1.e-5.
        """
        self.step = step
        self.threshold = threshold
        self.rad2deg = 180. / np.pi

    def integrate(self, spectrum, world, ray, primitive, material, start_point, end_point, world_to_primitive, primitive_to_world):
        start = start_point.transform(world_to_primitive)  # start point in local coordinates
        end = end_point.transform(world_to_primitive)  # end point in local coordinates
        direction = start.vector_to(end)  # direction of integration
        length = direction.length  # integration length
        if length < 0.1 * self.step:  # return if ray's path is too short
            return spectrum
        direction = direction.normalise()  # normalized direction
        n = max(2, int(length / self.step))  # number of points along ray's trajectory
        t, dt = np.linspace(0, length, n, retstep=True, endpoint=False)  # regulary scattered points along ray's trajectory
        t = t[:-1] + 0.5 * dt  # moving them into the centers of the intervals (and removing the last point)
        x = start.x + direction.x * t  # x coordinates of the points
        y = start.y + direction.y * t  # y coordinates of the points
        z = start.z + direction.z * t  # z coordinates of the points
        iz = (z / material.dz).astype(int)  # indices of grid cell along Z direction, in which the points are located
        r = np.sqrt(x * x + y * y)  # R coordinates of the points
        ir = ((r - material.rmin) / material.dr).astype(int)  # indices of grid cell along R direction, in which the points are located
        # the emitter is located in the bounding cylinders such a way that we always have:
        # 0 <= ir < emiss.shape[0] and 0 <= iz < emiss.shape[-1], so the boundary check is not requred
        phi = np.arctan2(y, x)  # phi coordinates of the points (in degrees)
        phi[phi < 0] += 2. * np.pi  # making them all in [0, 2pi) interval
        if material.is3d:  # material is a 3D emitter
            phi_sector = (self.rad2deg * phi) % material.phimax  # phi coordinates in [0, phimax)  sector
            iphi = (phi_sector / material.dphi).astype(int)  # indices of grid cell along phi direction, in which the points are located
            emiss = material.emiss[ir, iphi, iz]  # emission profile along ray's trajectory
        else:  # material is RegularRZEmitter
            emiss = material.emiss[ir, iz]  # emission profile along ray's trajectory
        # getting the indices of the points with non-zero emission and temperature
        indx, = np.where((emiss > self.threshold))
        if not indx.size:  # return if no such points
            return spectrum
        emiss = emiss[indx]  # getting the points with zero emission or temperature
        phi = phi[indx]
        ir = ir[indx]
        iz = iz[indx]
        lineshape0 = material.lineshape[0][ir, iz]  # spectral line shape along ray's trajectory if emitted perpendicular to magnetic field
        lineshape1 = material.lineshape[1][ir, iz]  # spectral line shape along ray's trajectory if emitted parallel to magnetic field
        bnr = material.bn[ir, iz, 0]  # R-component of magnetic field's direction along ray's trajectory
        bnphi = material.bn[ir, iz, 1]  # phi-component of magnetic field's direction along ray's trajectory
        bnz = material.bn[ir, iz, 2]  # Z-component of magnetic field's direction along ray's trajectory
        bnx = bnr * np.cos(phi) - bnphi * np.sin(phi)  # x-component of magnetic field's direction along ray's trajectory
        bny = bnr * np.sin(phi) + bnphi * np.cos(phi)  # y-component of magnetic field's direction along ray's trajectory
        # profile of the cosine of the angle between ray's trajectory and magnetic field direction
        costheta = direction.x * bnx + direction.y * bny + direction.z * bnz
        costheta2 = costheta * costheta
        sintheta2 = 1. - costheta2
        # volume integral along ray's trajectory (spectral intensity)
        spectrum.samples += (emiss[:, None] * (lineshape0 * sintheta2[:, None] + lineshape1 * costheta2[:, None])).sum(0) * dt

        return spectrum


class RegularRPhiZEmitter(InhomogeneousVolumeEmitter):
    """ Emitter defined on a regular 3D grid in R, phi, Z coordinates (or 2D grid in R, Z coordinates).
        The emitter is periodic in toroidal direction."""

    def __init__(self, emiss, rmin, dr, dz, phimax=0, dphi=0,
                 spectralfunc=None, wavelength=None, ti=None, bmag=None, bn=None,
                 integrator=None):
        """ Parameters
            ----------
            emiss : 3d or 2d numpy.ndarray
                Emissivity(R,phi,Z) or Emissivity(R,Z) defined on a regular 3D or 2D gird.
            rmin : float
                Lower bound of RPhiZ grid in R directon (in meters) (emiss[0, :, :] is defined between rmin and rmin + dr).
            dr : float
                Grid step in R direction (in meters)
            dz : float
                Grid step in Z direction (in meters)
            phimax : float, optional
                Upper bound of RPhiZ grid in phi directon (in degree) and a period in toroidal direction.
                (emiss[:, -1, :] is defined between phimax - dphi and phimax).
                It is assumed that the grid starts from 0 in phi direction. Just rotate the material if not.
                Used only in the case of 3D emitter (emiss.ndim > 2).
            dphi : float, optional
                Grid step in phi direction (in degree)
                Used only in the case of 3D emitter (emiss.ndim > 2).
            spectralfunc : callable, optional
                A callable that returns spectral emission profile parallel and perpendicular to magnetic field's direction:
                    spectralfunc(wavelengths, ti, bmag, along).
                        wavelengths: wavelength array
                        ti: 1D temperature profile
                        bmag: 1D magnetic field strength profile
                        along: 1 for the parallel component and 0 for the perpendicular component
            ti : 2d numpy.ndarray, optional
                Ion (or atom) temperature (in eV) defined on a regular 2D gird (RZ-plane).
                Required when used with ZeemanRegularRPhiZIntegrator.
            bmag : 2d numpy.ndarray, optional
                Magnetic field strngth (in T) defined on a regular 2D gird  (RZ-plane).
                Required when used with ZeemanRegularRPhiZIntegrator.
            bn : 3d numpy.ndarray, optional
                Magnetic field direction (normalized) defined on a regular 2D gird  (RZ-plane).
                bn[:, :, 0] - R-component, bn[:, :, 1] - phi-component, bn[:, :, 2] - Z-component.
                bn[:, :, 0]**2 + bn[:, 1]**2 + bn[:, :, 2]**2 = 1
                Required when used with ZeemanRegularRPhiZIntegrator.
            integrator : VolumeIntegrator, optional
                Volume integrator: MonoRegularRPhiZIntegrator (defualt) or ZeemanRegularRPhiZIntegrator.
        """
        integrator = integrator or RegularRPhiZIntegrator(0.25 * min(dr, dz))
        super().__init__(integrator)
        if emiss.ndim > 2 and not phimax * dphi:
            raise ValueError('phimax and dphi must be non-zero for 3D emitters')
        self.indx = 0
        self.is3d = emiss.ndim > 2
        self.emiss = emiss
        self.rmin = rmin
        self.dr = dr
        self.dz = dz
        self.phimax = phimax
        self.dphi = dphi
        self.bn = bn
        self.lineshape = None
        if all(v is not None for v in (spectralfunc, wavelength, ti, bmag, bn)):
            self.lineshape = self.calc_lineshape(spectralfunc, wavelength, ti, bmag)

    def calc_lineshape(self, spectralfunc, wavelength, ti, bmag):
        lineshape = [np.zeros((ti.shape[0], ti.shape[1], wavelength.size)),
                     np.zeros((ti.shape[0], ti.shape[1], wavelength.size))]
        indx = np.where(ti > 0.001)  # grids may have zero temperature in some areas
        for i in range(2):
            lineshape[i][indx] = spectralfunc(wavelength, ti[indx], bmag[indx], i)

        return lineshape


class DiscreteSpectralRPhiZEmitter(InhomogeneousVolumeEmitter):
    """ Discrete spectral emitter defined on a regular 3D grid in R, phi, Z coordinates (or 2D grid in R, Z coordinates).
        The emitter is periodic in toroidal direction.
        This emitter allows to calculate the radiance from multiple spectral lines in a single run of camera.observe() function.
        Note that spectral line shapes are not calculated, only the total intensity of the spectral lines.
    """

    def __init__(self, emiss, lines_wavelength, min_wavelength, max_wavelength, bins, rmin, dr, dz,
                 phimax=0, dphi=0, integrator=None):
        """ Parameters
            ----------
            emiss : 4d or 3d numpy.ndarray
                Emissivity(R, phi, Z, lines_wavelength) or Emissivity(R, Z, lines_wavelength)
                defined on a regular 3D or 2D gird for the set of spectral lines.
            lines_wavelength : 1d numpy.ndarray
                Wavelengths of the spectral lines for which the emissivity is provided.
            min_wavelength : float
                Lower wavelength bound for sampled spectral range. Must be equal to camera.min_wavelength.
            max_wavelength : float
                Upper wavelength bound for sampled spectral range. Must be equal to camera.max_wavelength.
            bins : int
                The number of spectral samples over the wavelength range. Must be equal to camera.spectral_bins.
            rmin : float
                Lower bound of RPhiZ grid in R directon (in meters) (emiss[0, :, :] is defined between rmin and rmin + dr).
            dr : float
                Grid step in R direction (in meters)
            dz : float
                Grid step in Z direction (in meters)
            phimax : float, optional
                Upper bound of RPhiZ grid in phi directon (in degree) and a period in toroidal direction.
                (emiss[:, -1, :] is defined between phimax - dphi and phimax).
                It is assumed that the grid starts from 0 in phi direction. Just rotate the material if not.
                Used only in the case of 3D emitter (emiss.ndim > 2).
            dphi : float, optional
                Grid step in phi direction (in degree)
                Used only in the case of 3D emitter (emiss.ndim > 2).
            integrator : VolumeIntegrator, optional
                Volume integrator: RegularRPhiZIntegrator (defualt).
        """
        integrator = integrator or RegularRPhiZIntegrator(0.25 * min(dr, dz))
        super().__init__(integrator)
        if emiss.ndim > 3 and not phimax * dphi:
            raise ValueError('phimax and dphi must be non-zero for 3D emitters')
        delta_wl = (max_wavelength - min_wavelength) / bins
        # The indeces of spectral bins corresponding to wavelengths in lines_wavelength array
        self.indx = ((lines_wavelength - min_wavelength) / delta_wl).astype(int)
        if np.unique(self.indx).size < self.indx.size:
            warnings.warn('Multiple spectral lines fit into a single spectral bin. Increase the number of spectral bins to avoid this.')
        self.is3d = emiss.ndim > 3
        self.emiss = emiss
        self.rmin = rmin
        self.dr = dr
        self.dz = dz
        self.phimax = phimax
        self.dphi = dphi


if __name__ == "__main__":
    pass
