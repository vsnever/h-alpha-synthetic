# -*- coding: utf-8 -*-
from random import shuffle
import numpy as np
from raysect.optical.observer.base import FrameSampler1D, FrameSampler2D
from raysect.optical.observer.pipeline import SpectralRadiancePipeline1D, SpectralPowerPipeline1D, SpectralRadiancePipeline2D, SpectralPowerPipeline2D


class SequentialFullFrameSampler1D(FrameSampler1D):

    def generate_tasks(self, pixels):

        tasks = []
        for pixel in range(pixels):
            tasks.append((pixel, ))

        return tasks


class SequentialMaskedSampler1D(FrameSampler1D):

    def __init__(self, mask):
        self.mask = mask

    def generate_tasks(self, pixels):

        tasks = []
        for pixel in range(pixels):
            if self.mask[pixel]:
                tasks.append((pixel, ))

        return tasks


class SequentialFullFrameSampler2D(FrameSampler2D):

    def generate_tasks(self, pixels):

        tasks = []
        nx, ny = pixels
        for iy in range(ny):
            for ix in range(nx):
                tasks.append((ix, iy))

        return tasks


class SequentialMaskedSampler2D(FrameSampler2D):

    def __init__(self, mask):
        self.mask = mask

    def generate_tasks(self, pixels):

        tasks = []
        nx, ny = pixels
        for iy in range(ny):
            for ix in range(nx):
                if self.mask[ix, iy]:
                    tasks.append((ix, iy))

        return tasks


class SpectralAdaptiveSampler1D(FrameSampler1D):
    """
    The adaptive sampler to use with spectral pipelines. Based on MonoAdaptiveSampler1D from Raysect.
    """

    def __init__(self, pipeline, fraction=0.2, ratio=10.0, min_samples=1000, cutoff=0.0):

        if not isinstance(pipeline, (SpectralPowerPipeline1D, SpectralRadiancePipeline1D)):
            raise TypeError('Sampler only compatible with SpectralPowerPipeLine1D or SpectralRadiancePipeline1D pipelines.')

        self.pipeline = pipeline
        self.fraction = fraction
        self.ratio = ratio
        self.min_samples = min_samples
        self.cutoff = cutoff

    def generate_tasks(self, pixels):

        frame = self.pipeline.frame
        if frame is None:
            # no frame data available, generate tasks for the full frame
            return self._full_frame(pixels)

        # sanity check
        if pixels != frame.length:
            raise ValueError('The pixel geometry passed to the frame sampler is inconsistent with the pipeline frame size.')

        min_samples = max(self.min_samples, frame.samples.max() / self.ratio)
        error = frame.errors()

        # calculated normalised standard error
        normalised_spectral = np.zeros(frame.shape)
        indx = np.where(frame.mean > 0)
        normalised_spectral[indx] = error[indx] / frame.mean[indx]
        normalised = normalised_spectral.max(1)

        # locate error value corresponding to fraction of frame to process
        percentile_error = np.percentile(normalised, (1 - self.fraction) * 100)

        # build tasks
        samples = frame.samples.min(1)
        indx, = np.where((samples < min_samples) + (normalised > max(self.cutoff, percentile_error)))
        tasks = [(pixel, ) for pixel in indx]

        # perform tasks in random order so that image is assembled randomly rather than sequentially
        shuffle(tasks)

        return tasks

    def _full_frame(self, pixels):

        tasks = [(pixel, ) for pixel in range(pixels)]

        # perform tasks in random order so that image is assembled randomly rather than sequentially
        shuffle(tasks)

        return tasks


class SpectralAdaptiveSampler2D(FrameSampler2D):
    """
    The adaptive sampler to use with spectral pipelines. Based on MonoAdaptiveSampler2D from Raysect
    """

    def __init__(self, pipeline, fraction=0.2, ratio=10.0, min_samples=1000, cutoff=0.0):

        if not isinstance(pipeline, (SpectralPowerPipeline2D, SpectralRadiancePipeline2D)):
            raise TypeError('Sampler only compatible with SpectralPowerPipeLine2D or SpectralRadiancePipeline2D pipelines.')

        self.pipeline = pipeline
        self.fraction = fraction
        self.ratio = ratio
        self.min_samples = min_samples
        self.cutoff = cutoff

    def generate_tasks(self, pixels):

        frame = self.pipeline.frame
        if frame is None:
            # no frame data available, generate tasks for the full frame
            return self._full_frame(pixels)

        # sanity check
        if pixels != (frame.nx, frame.ny):
            raise ValueError('The pixel geometry passed to the frame sampler is inconsistent with the pipeline frame size.')

        min_samples = max(self.min_samples, frame.samples.max() / self.ratio)
        error = frame.errors()

        # calculated normalised standard error
        normalised_spectral = np.zeros(frame.shape)
        indx = np.where(frame.mean > 0)
        normalised_spectral[indx] = error[indx] / frame.mean[indx]
        normalised = normalised_spectral.max(2)

        # locate error value corresponding to fraction of frame to process
        percentile_error = np.percentile(normalised, (1 - self.fraction) * 100)

        # build tasks
        samples = frame.samples.min(2)
        indx = np.where((samples < min_samples) + (normalised > max(self.cutoff, percentile_error)))
        tasks = [(x, y) for x, y in zip(*indx)]

        # perform tasks in random order so that image is assembled randomly rather than sequentially
        shuffle(tasks)

        return tasks

    def _full_frame(self, pixels):

        nx, ny = pixels
        tasks = [(x, y) for x in range(nx) for y in range(ny)]

        # perform tasks in random order so that image is assembled randomly rather than sequentially
        shuffle(tasks)

        return tasks


if __name__ == "__main__":
    pass
