# -*- coding: utf-8 -*-
from __future__ import print_function
import numpy as np
from raysect.optical import Point3D, Vector3D
from synthdiag import FoV


class FoVRaysect(FoV):

    def vectors_for_camera(self, origins_at_pupil=True):
        vector3dvec = np.vectorize(Vector3D)
        pixel_origins = np.empty((self.rays.shape[1], self.rays.shape[0]), dtype=object)
        pixel_directions = np.empty((self.rays.shape[1], self.rays.shape[0]), dtype=object)
        if origins_at_pupil:
            pixel_origins[:, :] = Point3D(self.pupil[0], self.pupil[1], self.pupil[2])
        else:
            pixel_origins[:, :] = Point3D(0, 0, 0)
        pixel_directions[:, :] = vector3dvec(self.rays[::-1, :, 0].T, self.rays[::-1, :, 1].T, self.rays[::-1, :, 2].T)

        return pixel_origins, pixel_directions


if __name__ == "__main__":
    pass
