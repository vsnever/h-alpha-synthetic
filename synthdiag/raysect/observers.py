# -*- coding: utf-8 -*-
# Created by Vladislav Neverov (NRC "Kurchatov Institute") to use with Raysect and CHERAB Spectroscopy Modelling Framework
# The code is heavily-based on Raysect's pinhole camera
from raysect.optical.observer.sampler2d import FullFrameSampler2D
from raysect.optical.observer.pipeline import RGBPipeline2D
from raysect.optical.observer.sampler2d import RGBAdaptiveSampler2D
from raysect.core import Point3D, Vector3D
from raysect.optical.observer.base import Observer2D
import numpy as np


# Contains the code from Raysect's PinholeCamera class
class FovCamera(Observer2D):
    """ Angular field-of-view observer.

    A camera that launches rays from the observer's origin point over a specified field of view in spherical coordinates.
    Unlike pinhole camera, each pixel of the final image represents a solid angle of collection, not a rectangle on the plane.

    :param tuple pixels: A tuple of pixel dimensions for the camera, i.e. (512, 512).
    :param tuple fov: The field of view of the camera in degrees in horizontal and vertical directions (default=(90, 90) degrees).
    :param float sensitivity: The sensitivity of each pixel (default=1.0)
    :param FrameSampler2D frame_sampler: The frame sampling strategy, defaults to adaptive
      sampling (i.e. extra samples for noisier pixels).
    :param list pipelines: The list of pipelines that will process the spectrum measured
      at each pixel by the camera (default=RGBPipeline2D()).
    :param kwargs: **kwargs and properties from Observer2D and _ObserverBase.
    """

    def __init__(self, pixels, fov=(90., 90.), sensitivity=1.0, frame_sampler=None, pipelines=None, parent=None, transform=None, name=None):

        # defaults to an adaptively sampled RGB pipeline
        if not pipelines and not frame_sampler:
            rgb = RGBPipeline2D()
            pipelines = [rgb]
            frame_sampler = RGBAdaptiveSampler2D(rgb)
        else:
            pipelines = pipelines or [RGBPipeline2D()]
            frame_sampler = frame_sampler or FullFrameSampler2D()

        super().__init__(pixels, frame_sampler, pipelines, parent=parent, transform=transform, name=name)

        # note that the fov property triggers a call to _update_image_geometry()
        self.fov = fov
        self.sensitivity = sensitivity

    @property
    def fov(self):
        return self._fov

    @fov.setter
    def fov(self, value):
        if len(value) != 2:
            raise ValueError("Fov must be a 2 element tuple defining the field-of-view in horizontal (phi) and vertical (theta) directions")
        if value[0] <= 0 or value[0] >= 180 or value[1] <= 0 or value[1] >= 180:
            raise ValueError("The field-of-view angle must lie in the range (0, 180).")
        self._fov = value
        self._update_image_geometry()

    @property
    def sensitivity(self):
        return self._sensitivity

    @sensitivity.setter
    def sensitivity(self, value):
        if value <= 0:
            raise ValueError("Sensitivity must be greater than zero.")
        self._sensitivity = value

    def _update_image_geometry(self):
        self.phi, self.dphi = np.linspace(-0.5 * self._fov[0], 0.5 * self._fov[0], self.pixels[0] + 1, retstep=True)
        self.theta, self.dtheta = np.linspace(0.5 * self._fov[1], -0.5 * self._fov[1], self.pixels[1] + 1, retstep=True)

    def _generate_rays(self, x, y, template, ray_count):
        deg2rad = np.pi / 180.
        phi = deg2rad * self.phi[x]
        theta = deg2rad * self.theta[y]
        dphi = deg2rad * self.dphi
        dtheta = deg2rad * self.dtheta
        phi_samples = phi + np.random.uniform(0, dphi, ray_count)
        theta_samples = theta + np.random.uniform(0, dtheta, ray_count)
        xend = -np.sin(phi_samples) * np.cos(theta_samples)
        yend = np.sin(theta_samples)
        zend = np.cos(phi_samples) * np.cos(theta_samples)
        # assemble rays
        rays = []
        for i in range(ray_count):
            origin = Point3D(0, 0, 0)
            direction = Vector3D(xend[i], yend[i], zend[i]).normalise()
            ray = template.copy(origin, direction)
            rays.append((ray, 1.0))

        return rays

    def _pixel_sensitivity(self, x, y):
        return self._sensitivity
