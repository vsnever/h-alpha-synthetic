# -*- coding: utf-8 -*-
# Created by Vladislav Neverov (NRC "Kurchatov Institute") to use with Raysect and CHERAB Spectroscopy Modelling Framework
import numpy as np
from raysect.optical.material import VolumeIntegrator, InhomogeneousVolumeEmitter


class RtmRegularRPhiZIntegrator(VolumeIntegrator):
    """ Calculates ray transfer matrix (or geometry matrix) for the light sources defined on a regular grid
        in cylindrical coordinate system: (R, phi, Z). The value for each light source is stored in respective spectral bin.
        Custom geometry may be applied to the light sources via the material.map array (see RtmRegularRPhiZEmitter),
        which can map multiple voxels in (R, phi, Z) space to a single spectral bin (and thus to a single light source).
        It is assumed that emitter is periodic in phi direction with a period equal to material.phimax.
        The path length of the ray passing throuh the voxel is calculated approximately and the accuracy depends on the integration step.
    """

    def __init__(self, step=0.001):
        """ Parameters
            ----------
            step : float, optional
                Integration step (in meters). Default: 0.001.
        """
        self.step = step
        self.rad2deg = 180. / np.pi

    def integrate(self, spectrum, world, ray, primitive, material, start_point, end_point, world_to_primitive, primitive_to_world):
        start = start_point.transform(world_to_primitive)  # start point in local coordinates
        end = end_point.transform(world_to_primitive)  # end point in local coordinates
        direction = start.vector_to(end)  # direction of integration
        length = direction.length  # integration length
        if length < 0.1 * self.step:  # return if ray's path is too short
            return spectrum
        direction = direction.normalise()  # normalized direction
        n = max(2, int(length / self.step))  # number of points along ray's trajectory
        t, dt = np.linspace(0, length, n, retstep=True)  # regulary scattered points along ray's trajectory and integration step
        t = t[:-1] + 0.5 * dt  # moving them into the centers of the intervals (and removing the last point)
        x = start.x + direction.x * t  # x coordinates of the points
        y = start.y + direction.y * t  # y coordinates of the points
        z = start.z + direction.z * t  # z coordinates of the points
        iz = (z / material.dz).astype(int)  # Z-indices of grid cells, in which the points are located
        r = np.sqrt(x * x + y * y)  # R coordinates of the points
        ir = ((r - material.rmin) / material.dr).astype(int)  # R-indices of grid cell, in which the points are located
        if material.map.ndim > 2:  # 3D grid
            phi = self.rad2deg * np.arctan2(y, x)  # phi coordinates of the points (in degrees)
            phi[phi < 0] += 360.  # making them all in [0, 360) interval
            phi = phi % material.phimax  # moving into the [0, phimax) sector (periodic emitter)
            iphi = (phi / material.dphi).astype(int)  # phi-indices of grid cell, in which the points are located
            i0 = material.map[ir, iphi, iz]  # light source indeces in spectral array
        else:  # 2D grid (RZ-plane)
            i0 = material.map[ir, iz]  # light source indeces in spectral array
        i, counts = np.unique(i0[i0 > -1], return_counts=True)  # exclude voxels for which i0 == -1
        spectrum.samples[i] += counts * dt

        return spectrum


class RtmRegularXYZIntegrator(VolumeIntegrator):
    """ Calculates ray transfer matrix (or geometry matrix) for the light sources defined on a regular grid
        in Cartesian coordinate system. The value for each light source is stored in respective spectral bin.
        Custom geometry may be applied to the light sources via the material.map array (see RtmRegularXYZEmitter),
        which can map multiple voxels in (X, Y, Z) space to a single spectral bin (and thus to a single light source).
        The path length of the ray passing throuh the voxel is calculated approximately and the accuracy depends on the integration step.
    """

    def __init__(self, step=0.001):
        """ Parameters
            ----------
            step : float, optional
                Integration step (in meters). Default: 0.001.
        """
        self.step = step
        self.rad2deg = 180. / np.pi

    def integrate(self, spectrum, world, ray, primitive, material, start_point, end_point, world_to_primitive, primitive_to_world):
        start = start_point.transform(world_to_primitive)  # start point in local coordinates
        end = end_point.transform(world_to_primitive)  # end point in local coordinates
        direction = start.vector_to(end)  # direction of integration
        length = direction.length  # integration length
        if length < 0.1 * self.step:  # return if ray's path is too short
            return spectrum
        direction = direction.normalise()  # normalized direction
        n = max(2, int(length / self.step))  # number of points along ray's trajectory
        t, dt = np.linspace(0, length, n, retstep=True)  # regulary scattered points along ray's trajectory and integration step
        t = t[:-1] + 0.5 * dt  # moving them into the centers of the intervals (and removing the last point)
        x = start.x + direction.x * t  # x coordinates of the points
        y = start.y + direction.y * t  # y coordinates of the points
        z = start.z + direction.z * t  # z coordinates of the points
        ix = (x / material.dx).astype(int)  # X-indices of grid cells, in which the points are located
        iy = (y / material.dy).astype(int)  # Y-indices of grid cells, in which the points are located
        iz = (z / material.dz).astype(int)  # Z-indices of grid cells, in which the points are located
        i0 = material.map[ix, iy, iz]  # light source indeces in spectral array
        i, counts = np.unique(i0[i0 > -1], return_counts=True)  # exclude voxels for which i0 == -1
        spectrum.samples[i] += counts * dt

        return spectrum


class RtmRegularRPhiZEmitter(InhomogeneousVolumeEmitter):
    """ Unity emitter defined on a regular 2D (RZ plane) or 3D (R, phi, Z) grid, which can be used to calculate
        ray transfer matrix (geometry matrix)"""

    def __init__(self, nr, nz, dr, dz, rmin, nphi=0, dphi=0, phimax=0, voxel_map=None, mask=None, integrator=None):
        """ Parameters
            ----------
            nr : int
                Number of grid points in R direction
            nz : int
                Number of grid points in Z direction
            dr : float
                Grid step in R direction (in meters)
            dz : float
                Grid step in Z direction (in meters)
            rmin : float
                Lower bound of grid in R direction (in meters).
            nphi : int, optional
                Number of grid points in Phi direction. Default: 0 (2D grid).
            dphi : float, optional
                Grid step in phi direction (in degree)
                Used only if nphi > 0.
            phimax : float, optional
                Upper bound of RPhiZ grid in phi directon (in degree) and a period in toroidal direction.
                (emiss[:, -1, :] is defined between phimax - dphi and phimax).
                It is assumed that the grid starts from 0 in phi direction. Just rotate the emitter if not.
                Used only if nphi > 0.
            voxel_map : 2D (nphi = 0) or 3D (nphi > 0) numpy.ndarray of integers, optional
                Array that contain indeces of light sources. It maps voxels in (R, phi, Z) space to respective light sources.
                The voxels with identical indeces in voxel_map array form a single light source.
                If voxel_map[ir, iphi, iz] == -1, the voxel (ir, iphi, iz) will not be mapped to any light source.
                This parameters allows to apply custom geometry (pixelated though) to the light sources.
            mask : 2D (nphi = 0) or 3D (nphi > 0) numpy.ndarray of booleans, optional
                Mask array. Ignored if voxel_map is provided.
                The ray tranfer matrix will be calculated only for those voxels for which mask == True.
            integrator : VolumeIntegrator
                RtmRegularRPhiZIntegrator.
        """
        integrator = integrator or RtmRegularRPhiZIntegrator(0.1 * min(dr, dz))
        super().__init__(integrator)
        if nphi and not dphi * phimax:
            raise ValueError('phimax and dphi must be non-zero for 3D grid')
        self.rmin = rmin
        self.phimax = phimax
        self.dr = dr
        self.dphi = dphi
        self.dz = dz
        self.nr = nr
        self.nphi = nphi
        self.nz = nz
        self.map = voxel_map or self.map_from_mask(mask)
        self.bins = self.map.max() + 1

    def map_from_mask(self, mask=None):
        if mask is None:
            mask = np.ones((self.nr, self.nphi, self.nz), dtype=np.bool) if self.nphi else np.ones((self.nr, self.nz), dtype=np.bool)
        voxel_map = -1 * np.ones(mask.shape, dtype=int)
        voxel_map[mask] = np.arange(mask.sum(), dtype=int)

        return voxel_map


class RtmRegularXYZEmitter(InhomogeneousVolumeEmitter):
    """ Unity emitter defined on a regular 3D (X, Y, Z) grid, which can be used to calculate ray transfer matrices"""

    def __init__(self, nx, ny, nz, dx, dy, dz, voxel_map=None, mask=None, integrator=None):
        """ Parameters
            ----------
            nx : int
                Number of grid points in X direction
            ny : int
                Number of grid points in Y direction
            nz : int
                Number of grid points in Z direction
            dx : float
                Grid step in X direction (in meters)
            dy : float
                Grid step in Y direction (in meters)
            dz : float
                Grid step in Z direction (in meters)
            voxel_map : 3D numpy.ndarray of integers, optional
                Array that contain indeces of light sources. It maps voxels in (X, Y, Z) space to respective light sources.
                The voxels with identical indeces in voxel_map array form a single light source.
                If voxel_map[ix, iy, iz] == -1, the voxel (ix, iy, iz) will not be mapped to any light source.
                This parameters allows to apply custom geometry (pixelated though) to the light sources.
            mask : 3D numpy.ndarray of booleans, optional
                Mask array. Ignored if voxel_map is provided.
                The ray tranfer matrix will be calculated only for those voxels for which mask == True.
            integrator : VolumeIntegrator
                RtmRegularXYZIntegrator.
        """
        integrator = integrator or RtmRegularXYZIntegrator(0.1 * min(dx, dy, dz))
        super().__init__(integrator)
        self.nx = nx
        self.ny = ny
        self.nz = nz
        self.dx = dx
        self.dy = dy
        self.dz = dz
        self.map = voxel_map or self.map_from_mask(mask)
        self.bins = self.map.max() + 1

    def map_from_mask(self, mask=None):
        mask = mask or np.ones((self.nx, self.ny, self.nz), dtype=np.bool)
        voxel_map = -1 * np.ones(mask.shape, dtype=int)
        voxel_map[mask] = np.arange(mask.sum(), dtype=int)

        return voxel_map


if __name__ == "__main__":
    pass
