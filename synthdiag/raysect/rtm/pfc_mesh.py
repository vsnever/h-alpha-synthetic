# -*- coding: utf-8 -*-
from __future__ import print_function
import os
import json
from numpy import array
import raysect.optical.library.metal as libmetal
from raysect.optical import rotate, InterpolatedSF
from raysect.optical.material import AbsorbingSurface
from raysect.primitive.mesh import Mesh
from .roughconductor import RtmRoughConductor


class _DataLoader(RtmRoughConductor):

    def __init__(self, filename, roughness):

        with open(os.path.join(os.path.dirname(libmetal.__file__), "data", filename + ".json")) as f:
            data = json.load(f)

        wavelength = array(data['wavelength'])
        index = InterpolatedSF(wavelength, array(data['index']))
        extinction = InterpolatedSF(wavelength, array(data['extinction']))

        super().__init__(index, extinction, roughness)


class RtmRoughBeryllium(_DataLoader):
    """Beryllium metal material."""

    def __init__(self, roughness):
        super().__init__("beryllium", roughness)


class RtmRoughIron(_DataLoader):
    """Iron metal material."""

    def __init__(self, roughness):
        super().__init__("iron", roughness)


class RtmRoughTungsten(_DataLoader):
    """Tungsten metal material."""

    def __init__(self, roughness):
        super().__init__("tungsten", roughness)


def load_pfc_mesh(world, path=None, reflections=True, roughness={'Be': 0.26, 'W': 0.29, 'Ss': 0.13}, detailed=False):
    """ Loads ITER PFC mesh and connects it to Raysect World() instance.
        The coordinates of PFCs from the pfc_list are given in TGCS for the sector 1.
        Note that currently the entire first wall is obtained by copying and rotating the 1st sector 18 times.
        The coordinates of custom port plugs are given in TGCS.
        The coordinates of general port plugs are given in internal coordinate system (which can be
        transformed to TGCS for the sector 1 by rotating on 10 deg counter-clockwise over Z-axis).
        Parameters
        ----------
        world : Raysect World() instance
        path : string, optional
            Path to .rsm mesh files
        reflections : bool, optional
            Reflection on/off. Default: True.
        roughness : dict, optional
            Roughness dict for PFC materials ('Be', 'W', 'Ss').
        details : bool, optional
            Load the detailed wall model instead of the simple one. Default: False.
    """
    path = os.path.join(os.path.dirname(__file__), '../ITER_PFC') or path
    path_fw = os.path.join(path, 'detailed') if detailed else os.path.join(path, 'simple')
    path_pp = os.path.join(path, 'portplugs')
    if detailed:
        pfc_list = ['blanket_BMs_1-9', 'blanket_BMs_10-18', 'divertor']
    else:
        pfc_list = ['blanket', 'divertor', 'protrusion_caps_backsonly', 'protrusions_wo_caps', 'divertor_outer_reflector_plate']
    gen_pp_sectors = {'EPP': (2, 4, 5, 6, 7, 13, 14, 15, 16, 18), 'UPP': (12, 13, 14, 15, 16)}
    custom_pp_sectors = {'EPP': (1, 3, 8, 9, 10, 11, 12, 17), 'UPP': (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 17, 18)}
    ncopy = {'blanket': 18, 'blanket_BMs_1-9': 18, 'blanket_BMs_10-18': 18, 'divertor': 18,
             'protrusion_caps_backsonly': 18, 'protrusions_wo_caps': 18, 'divertor_outer_reflector_plate': 54}
    if reflections:
        materials = {'blanket': RtmRoughBeryllium(roughness['Be']), 'blanket_BMs_1-9': RtmRoughBeryllium(roughness['Be']),
                     'blanket_BMs_10-18': RtmRoughBeryllium(roughness['Be']), 'divertor': RtmRoughTungsten(roughness['W']),
                     'divertor_outer_reflector_plate': RtmRoughTungsten(roughness['W']), 'portplug': RtmRoughIron(roughness['Ss']),
                     'protrusion_caps_backsonly': RtmRoughBeryllium(roughness['Be']), 'protrusions_wo_caps': RtmRoughBeryllium(roughness['Be'])}
    else:
        materials = {'blanket': AbsorbingSurface(), 'blanket_BMs_1-9': AbsorbingSurface(),
                     'blanket_BMs_10-18': AbsorbingSurface(), 'divertor': AbsorbingSurface(),
                     'divertor_outer_reflector_plate': AbsorbingSurface(), 'portplug': AbsorbingSurface(),
                     'protrusion_caps_backsonly': AbsorbingSurface(), 'protrusions_wo_caps': AbsorbingSurface()}
    mesh = {}
    for pfc in pfc_list:
        mesh[pfc] = [Mesh.from_file(os.path.join(path_fw, '%s.rsm' % pfc), parent=world, material=materials[pfc])]
        angle = 360. / ncopy[pfc]
        for i in range(1, ncopy[pfc]):
            mesh[pfc].append(mesh[pfc][0].instance(parent=world, transform=rotate(0, 0, angle * i), material=materials[pfc]))
    for pp in ('EPP', 'UPP'):
        mkey = '%s_generic' % pp
        mesh[mkey] = [Mesh.from_file(os.path.join(path_pp, '%s_generic.rsm' % pp), parent=world, material=materials['portplug'],
                                     transform=rotate(0, 0, 20. * gen_pp_sectors[pp][0] - 10.))]
        for i in gen_pp_sectors[pp][1:]:
            mesh[mkey].append(mesh[mkey][0].instance(parent=world, transform=rotate(0, 0, 20. * i - 10.), material=materials['portplug']))
        mesh['%s_custom' % pp] = [Mesh.from_file(os.path.join(path_pp, '%s_%d.rsm' % (pp, i)),
                                                 parent=world, material=materials['portplug']) for i in custom_pp_sectors[pp]]

    return mesh


if __name__ == "__main__":
    pass
