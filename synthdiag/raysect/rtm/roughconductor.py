from math import pi, sqrt
from raysect.optical import Vector3D
from raysect.optical.material import RoughConductor


class RtmRoughConductor(RoughConductor):
    """ RoughConductor is optimized for calculation of ray transfer matrix (geometry matrix) when the spectral array
        is used to store the data for individual light sources and not the actual spectrum.
        The spectral array in this case contains ~ 10^5 - 10^6 spectral bins but the wavelengths for all of them are equal.
        The Fresnel indeces are equal for all spectral bins, so the unnecessary calculations must be avoided.
    """

    def evaluate_shading(self, world, ray, s_incoming, s_outgoing, w_reflection_origin, w_transmission_origin, back_face,
                         world_to_surface, surface_to_world):

        # outgoing ray is sampling incident light so s_outgoing = incident

        # material does not transmit
        if s_outgoing.z <= 0:
            return ray.new_spectrum()

        # ignore parallel rays which could cause a divide by zero later
        if s_incoming.z == 0:
            return ray.new_spectrum()

        # calculate half vector
        s_half = Vector3D(s_incoming.x + s_outgoing.x, s_incoming.y + s_outgoing.y, s_incoming.z + s_outgoing.z).normalise()

        # generate and trace ray
        reflected = ray.spawn_daughter(w_reflection_origin, s_outgoing.transform(surface_to_world))
        spectrum = reflected.trace(world)

        # evaluate lighting with Cook-Torrance bsdf (optimised)
        wavelength = 0.5 * (spectrum.min_wavelength + spectrum.max_wavelength)
        n = self.index.evaluate(wavelength)
        k = self.extinction.evaluate(wavelength)
        ci = s_half.dot(s_outgoing)
        mult = (self._d(s_half) * self._g(s_incoming, s_outgoing) * self._fresnel_conductor(ci, n, k)) / (4 * s_incoming.z)
        spectrum.samples *= mult

        return spectrum

    def _d(self, s_half):
        r2 = self.roughness * self.roughness
        h2 = s_half.z * s_half.z
        k = h2 * (r2 - 1) + 1

        return r2 / (pi * k * k)

    def _g(self, s_incoming, s_outgoing):
        # Smith's geometric shadowing model
        return self._g1(s_incoming) * self._g1(s_outgoing)

    def _g1(self, v):
        # Smith's geometric component (G1) for GGX distribution
        r2 = self.roughness * self.roughness
        return 2 * v.z / (v.z + sqrt(r2 + (1 - r2) * (v.z * v.z)))

    def _fresnel_conductor(self, ci, n, k):
        ci2 = ci * ci
        k0 = n * n + k * k
        k1 = k0 * ci2 + 1
        k2 = 2 * n * ci
        k3 = k0 + ci2

        return 0.5 * ((k1 - k2) / (k1 + k2) + (k3 - k2) / (k3 + k2))
