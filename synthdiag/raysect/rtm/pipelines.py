# -*- coding: utf-8 -*-
# Created by Vladislav Neverov (NRC "Kurchatov Institute") to use with Raysect and CHERAB Spectroscopy Modelling Framework
import numpy as np
from raysect.optical.observer.base import Pipeline0D, Pipeline1D, Pipeline2D, PixelProcessor

# Very simple but fast pipelines for ray transfer matrix (geometry matrix) calculation.
# Spectral array is used to store the data for individual unit light sources and not the actual spectrum.
# The spectral array may contain ~ 10,000 spectral bins but the wavelengths for all of them are equal.
# Standard error is not calculated in these pipelines, only the mean value.
# Dispersive rendering and adaptive sampling features are removed to improve the performance.


class RtmPipeline0D(Pipeline0D):

    def __init__(self, name=None):

        self.name = name or 'RtmPipeline0D'
        self.rtm = None
        self.samples = 0
        self.bins = 0

    def initialise(self, min_wavelength, max_wavelength, spectral_bins, spectral_slices, quiet):
        self.samples = 0
        self.bins = spectral_bins
        self.rtm = np.zeros(spectral_bins)

    def pixel_processor(self, slice_id):
        return RtmPixelProcessor(self.bins)

    def update(self, slice_id, packed_result, pixel_samples):
        self.samples += pixel_samples
        self.rtm += packed_result[0]

    def finalise(self):
        self.rtm /= self.samples


class RtmPipeline1D(Pipeline1D):

    def __init__(self, name=None):

        self.name = name or 'RtmPipeline1D'
        self.rtm = None
        self.pixels = None
        self.samples = 0
        self.bins = 0

    def initialise(self, pixels, pixel_samples, min_wavelength, max_wavelength, spectral_bins, spectral_slices, quiet):
        self.pixels = pixels
        self.samples = pixel_samples
        self.bins = spectral_bins
        self.rtm = np.zeros((pixels, spectral_bins))

    def pixel_processor(self, pixel, slice_id):
        return RtmPixelProcessor(self.bins)

    def update(self, pixel, slice_id, packed_result):
        self.rtm[pixel] = packed_result[0] / self.samples

    def finalise(self):
        pass


class RtmPipeline2D(Pipeline2D):

    def __init__(self, name=None):

        self.name = name or 'RtmPipeline2D'
        self.rtm = None
        self.pixels = None
        self.samples = 0
        self.bins = 0

    def initialise(self, pixels, pixel_samples, min_wavelength, max_wavelength, spectral_bins, spectral_slices, quiet):
        self.pixels = pixels
        self.samples = pixel_samples
        self.bins = spectral_bins
        self.rtm = np.zeros((pixels[0], pixels[1], spectral_bins))

    def pixel_processor(self, x, y, slice_id):
        return RtmPixelProcessor(self.bins)

    def update(self, x, y, slice_id, packed_result):
        self.rtm[x, y] = packed_result[0] / self.samples

    def finalise(self):
        pass


class RtmPixelProcessor(PixelProcessor):
    """
    PixelProcessor that stores ray transfer matrix for each pixel.
    """

    def __init__(self, bins):
        self.rtm = np.zeros(bins)

    def add_sample(self, spectrum, sensitivity):
        self.rtm += spectrum.samples * sensitivity

    def pack_results(self):
        return (self.rtm, 0)
