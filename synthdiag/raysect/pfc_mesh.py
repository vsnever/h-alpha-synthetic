# -*- coding: utf-8 -*-
from __future__ import print_function
import os
from raysect.optical import rotate, ConstantSF, SpectralFunction
from raysect.optical.library import RoughBeryllium, RoughTungsten, RoughIron
from cherab.tools.raytransfer import RoughBeryllium as RoughBerylliumRTM, RoughTungsten as RoughTungstenRTM, RoughIron as RoughIronRTM
from raysect.optical.material import AbsorbingSurface, Lambert, Blend
from raysect.primitive.mesh import Mesh


def load_pfc_mesh(world, path=os.path.join(os.path.dirname(__file__), 'ITER_PFC'), reflections=True,
                  roughness={'Be': 0.26, 'W': 0.29, 'Ss': 0.13}, lambert_ratio={'Be': 0, 'W': 0, 'Ss': 0},
                  lambert_albedo={'Be': 0.4689, 'W': 0.4328, 'Ss': 0.5095}, detailed=False, raytransfer=False,
                  open_ep_sector=None, open_up_sector=None):
    """
    Loads ITER PFC mesh and connects it to Raysect World() instance.
    The coordinates of PFCs from the pfc_list are given in TGCS for the sector 1.
    Note that currently the entire first wall is obtained by copying and rotating the 1st sector 18 times.
    The coordinates of custom port plugs are given in TGCS.
    The coordinates of general port plugs are given in internal coordinate system (which can be
    transformed to TGCS for the sector 1 by rotating on 10 deg counter-clockwise over Z-axis).

    Parameters
    ----------
    world : Raysect World() instance
    path : string, optional
        Path to .rsm mesh files
    reflections : bool, optional
        Reflection on/off. Default: True.
    roughness : dict, optional
        Dictionary with dimensionless roughness in Cook-Torrance BRDF for each PFC material: 'Be', 'W', 'Ss'.
    lambert_ratio : dict, optional
        Ratio of Lambertian component in total Cook-Torrance + Lambertian BRDF for each PFC material: 'Be', 'W', 'Ss'.
    lambert_albedo : dict, optional
        Albedo of Lambertian BRDF for each PFC material: 'Be', 'W', 'Ss'. Float value or SpectralFunction.
    detailed : bool, optional
        Load the detailed wall model instead of the simple one. Default: False.
    raytransfer : bool, optional
        Set this to True in case of ray transfer matrix (geometry matrix) calculation for speedup. Default: False.
    open_ep_sector : int, optional
        Number of sector in which the equatorial port is keeped open (without port plug) or None. Default: None.
    open_up_sector : int, optional
        Number of sector in which the upper port is keeped open (without port plug) or None. Default: None.
    """

    path_fw = os.path.join(path, 'detailed') if detailed else os.path.join(path, 'simple')
    path_pp = os.path.join(path, 'portplugs')

    if detailed:
        pfc_list = ['blanket_BMs_1-9', 'blanket_BMs_10-18', 'divertor']
    else:
        pfc_list = ['blanket', 'divertor', 'protrusion_caps_backsonly', 'protrusions_wo_caps', 'divertor_outer_reflector_plate']

    # Use generic meshes (with no cuts) for the port plugs in the sectors where the correct meshes are not available
    def_gen_pp_sectors = {'EPP': (2, 4, 5, 6, 7, 13, 14, 15, 16, 18), 'UPP': (12, 13, 14, 15, 16)}
    gen_pp_sectors = {'EPP': [sector for sector in def_gen_pp_sectors['EPP'] if sector != open_ep_sector],
                      'UPP': [sector for sector in def_gen_pp_sectors['UPP'] if sector != open_up_sector]}

    # Sectors where the correct meshes for the port plugs are available
    def_custom_pp_sectors = {'EPP': (1, 3, 8, 9, 10, 11, 12, 17), 'UPP': (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 17, 18)}
    custom_pp_sectors = {'EPP': [sector for sector in def_custom_pp_sectors['EPP'] if sector != open_ep_sector],
                         'UPP': [sector for sector in def_custom_pp_sectors['UPP'] if sector != open_up_sector]}

    # How many times each PFC element must be copy-pasted
    ncopy = {'blanket': 18, 'blanket_BMs_1-9': 18, 'blanket_BMs_10-18': 18, 'divertor': 18,
             'protrusion_caps_backsonly': 18, 'protrusions_wo_caps': 18, 'divertor_outer_reflector_plate': 54}

    if reflections:
        lambert = {}
        for key, value in lambert_albedo.items():
            if isinstance(value, SpectralFunction):
                lambert[key] = Lambert(value)
            else:
                lambert[key] = Lambert(ConstantSF(value))

        if raytransfer:
            cook_torrance = {'Be': RoughBerylliumRTM(roughness['Be']),
                             'W': RoughTungstenRTM(roughness['W']),
                             'Ss': RoughIronRTM(roughness['Ss'])}
        else:
            cook_torrance = {'Be': RoughBeryllium(roughness['Be']),
                             'W': RoughTungsten(roughness['W']),
                             'Ss': RoughIron(roughness['Ss'])}

        metals = {}
        for key, ratio in lambert_ratio.items():
            if ratio == 0:
                metals[key] = cook_torrance[key]
            elif ratio == 1.:
                metals[key] = lambert[key]
            else:
                metals[key] = Blend(cook_torrance[key], lambert[key], ratio=ratio, surface_only=True)

    else:
        metals = {'Be': AbsorbingSurface(), 'W': AbsorbingSurface(), 'Ss': AbsorbingSurface()}

    materials = {'blanket': metals['Be'], 'blanket_BMs_1-9': metals['Be'],
                 'blanket_BMs_10-18': metals['Be'], 'divertor': metals['W'],
                 'divertor_outer_reflector_plate': metals['W'], 'portplug': metals['Ss'],
                 'protrusion_caps_backsonly': metals['Be'], 'protrusions_wo_caps': metals['Be']}

    mesh = {}
    for pfc in pfc_list:
        mesh[pfc] = [Mesh.from_file(os.path.join(path_fw, '%s.rsm' % pfc), parent=world, material=materials[pfc])]  # master element
        angle = 360. / ncopy[pfc]  # rotate around Z by this angle
        for i in range(1, ncopy[pfc]):  # copies of the master element
            mesh[pfc].append(mesh[pfc][0].instance(parent=world, transform=rotate(0, 0, angle * i), material=materials[pfc]))

    for pp in ('EPP', 'UPP'):
        mkey = '%s_generic' % pp
        angle = 20. * gen_pp_sectors[pp][0] - 10.  # rotate around Z by this angle
        mesh[mkey] = [Mesh.from_file(os.path.join(path_pp, '%s_generic.rsm' % pp), parent=world, material=materials['portplug'],
                                     transform=rotate(0, 0, angle))]  # master generic port plug
        for i in gen_pp_sectors[pp][1:]:  # and it's copies
            angle = 20. * i - 10.  # rotate around Z by this angle
            mesh[mkey].append(mesh[mkey][0].instance(parent=world, transform=rotate(0, 0, angle), material=materials['portplug']))
        #  Custom port plugs are already in TGCS, no need to rotate
        mesh['%s_custom' % pp] = [Mesh.from_file(os.path.join(path_pp, '%s_%d.rsm' % (pp, i)),
                                                 parent=world, material=materials['portplug']) for i in custom_pp_sectors[pp]]

    return mesh


if __name__ == "__main__":
    pass
