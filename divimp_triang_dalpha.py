# -*- coding: utf-8 -*-
from __future__ import print_function
import sys
import numpy as np
from scipy.spatial import Delaunay
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
from scipy.interpolate import interp1d, LinearNDInterpolator


class TriMask:
    "Masks redundant triangles in Delaunay triangulation of DIVIMP mesh"

    def __init__(self, x, y, simplices):
        """ Parameters
            ----------
            x, y : 1d numpy.ndarray
                Coordinates of verticies.
            simplices : 2d numpy.ndarray of ints, shape (N, 3)
                Indices of the points forming the simplices in the triangulation
        """
        self.xv = x[simplices].T
        self.yv = y[simplices].T

    def calc_feature(self, feature='perimeter'):
        if feature == 'area':
            return self.calc_area()
        edgelengths = self.calc_edgelengths()
        if feature == 'perimeter':
            return edgelengths.sum(0)
        if feature == 'longedge':
            return edgelengths.max(0)

    def calc_area(self):
        return np.abs((self.xv[0] - self.xv[1]) * (self.yv[2] - self.yv[1]) -
                      (self.yv[0] - self.yv[1]) * (self.xv[2] - self.xv[1])) * 0.5

    def calc_edgelengths(self):
        edgelengths = np.zeros(self.xv.shape)
        ydiff = self.yv[0] - self.yv[1]
        xdiff = self.xv[0] - self.xv[1]
        edgelengths[0] = np.sqrt(xdiff * xdiff + ydiff * ydiff)
        ydiff = self.yv[0] - self.yv[2]
        xdiff = self.xv[0] - self.xv[2]
        edgelengths[2] = np.sqrt(xdiff * xdiff + ydiff * ydiff)
        ydiff = self.yv[2] - self.yv[1]
        xdiff = self.xv[2] - self.xv[1]
        edgelengths[1] = np.sqrt(xdiff * xdiff + ydiff * ydiff)

        return edgelengths

    def __call__(self, feature='perimeter', factor=3.5, factor_div=1.5):
        """ Creates the mask for redundant triangles.
            All triangles are compared by their features (perimeter, area or longest edge) with the average value.
            Triangles with 'feature > factor * average_feature' in the main chamber
            and 'feature > factor_div * average_feature' in the divertor are considered as redundant.

                Parameters
                ----------
                feature : string ('perimeter', 'area' or 'longedge')
                    Compare the triangles by their perimeters, areas or longest edges
                    Default: 'perimeter'
                factor : float, optional
                    Factor for the main chamber
                    Default: 3.5
                factor_div : float, optional
                    Factor for the divertor
                    Default: 1.5
        """
        zdiv = -3.7
        rdiv = 5.3
        feature_val = self.calc_feature(feature='perimeter')
        mean_feature = feature_val.mean()
        div_simplices = (self.yv.max(0) < zdiv) * (self.xv.max(0) < rdiv)
        mask = (feature_val > factor * mean_feature) + ((feature_val > factor_div * mean_feature) * div_simplices)

        return mask


def load_plasma_divimp(filename, firstline=28):
    """ Reads plasma data from DIVIMP text file

            Parameters
            ----------
            filename : string
                Path to DIVIMP file
            firstline : int, optional
                First line number with plasma data (starting with 0)
                Default: 28

            Returns
            -------
            data : 2d numpy.ndarray
                Plasma data
    """
    with open(filename, 'r') as f:
        text = f.read()
        i0 = text.find('{PLASMA}') + 8
        i1 = text.find('*', i0)
        nlines = int(text[i0:i1])
        lines = text.encode('utf-8').splitlines() if sys.version_info > (3, 0) else text.splitlines()  # check python version

        return np.genfromtxt(lines[firstline:firstline + nlines])


def grind_mesh(ln, r, z, profiles, factor=8, kind='linear'):
    """ Grinds original divimp mesh along the grid lines

            Parameters
            ----------
            ln : numpy.ndarray
                Grid line numbers
            r, z : numpy.ndarray
                R, Z coordinates
            profiles : dict of numpy.ndarray
                Plasma profiles
            factor : int, optional
                Grinding factor. For factor=X, the final mesh is X times denser.
                Default: 8
            kind : string, optional
                Kind of interpolation ('linear', 'cubic', etc.) to use along the grid lines for the profiles. See interp1d() manual.
                Default: 'linear'

            Returns
            -------
            (rnew, znew, profiles_new)
            rnew, znew : numpy.ndarray
                Refined R and Z coordinates
            profiles_new : dict of numpy.ndarray
                Plasma profiles on the refined mesh
    """
    rnew = np.zeros(factor * (r.size - 1) + 1)
    znew = np.zeros(factor * (r.size - 1) + 1)
    profiles_new = {key: np.zeros(factor * (r.size - 1) + 1) for key in profiles.keys()}
    for i in range(ln[0], ln[-1] + 1):
        points, = np.where(ln == i)
        n = points.size
        if not n:
            continue
        kind1 = 'linear' if n < 4 else kind
        ibegin = points[0]
        iend = points[-1] + 1
        p = np.linspace(0, n - 1, n)
        nnew = factor * (n - 1) + 1
        pnew = np.linspace(0, n - 1, nnew)
        ri = interp1d(p, r[ibegin:iend], kind='linear')
        zi = interp1d(p, z[ibegin:iend], kind='linear')
        rnew[factor * ibegin:factor * (iend - 1) + 1] = ri(pnew)
        znew[factor * ibegin:factor * (iend - 1) + 1] = zi(pnew)
        for key in profiles.keys():
            val = interp1d(p, profiles[key][ibegin:iend], kind=kind1)
            profiles_new[key][factor * ibegin:factor * (iend - 1) + 1] = val(pnew)
        # plt.plot(p, profiles['Dalpha'][ibegin:iend])
        # plt.plot(pnew, profiles_new['Dalpha'][factor * ibegin:factor * iend])
        # plt.show()
    if kind != 'linear':
        for key in profiles_new.keys():
            profiles_new[key][profiles_new[key] < 0] = 1.e-8

    return rnew, znew, profiles_new


def plot_tri(x, y, triangles, data, mask=None, label='', original_mesh=None):
    fig = plt.figure(figsize=(3.6, 4.9), dpi=140)
    loc = plticker.MultipleLocator(base=1.0)
    ax = fig.add_subplot(1, 1, 1)
    data2plot = np.log10(data)
    vmax = np.nanmax(data2plot)
    triplot = ax.tripcolor(x, y, triangles, data2plot, mask=mask, cmap='viridis', vmin=vmax - 8, vmax=vmax)
    ax.set_aspect('equal')
    ax.set_ylim(z.min() - 0.1, z.max() + 0.1)
    ax.set_xlim(4., r.max() + 0.1)
    ax.xaxis.set_major_locator(loc)
    ax.set_xlabel('R, m', labelpad=0.5)
    ax.set_ylabel('Z, m', labelpad=0)
    if original_mesh is not None:
        ln = original_mesh[0]
        r0 = original_mesh[1]
        z0 = original_mesh[2]
        for i in range(ln[0], ln[-1] + 1):
            points, = np.where(ln == i)
            ax.plot(r0[points], z0[points], '-r', lw=0.5, alpha=0.3)
    fig.subplots_adjust(left=0.13, right=0.75, bottom=0.08, top=0.97)
    pos1 = ax.get_position()
    cax = fig.add_axes([pos1.x0 + pos1.width + 0.04, pos1.y0, 0.04, pos1.height])
    cb = fig.colorbar(triplot, cax=cax)
    cb.set_label(label, labelpad=2.0)

    return fig


def plot_reg(x, y, data, label=''):
    fig = plt.figure(figsize=(3.6, 4.9), dpi=140)
    loc = plticker.MultipleLocator(base=1.0)
    ax = fig.add_subplot(1, 1, 1)
    data2plot = np.log10(data)
    vmax = np.nanmax(data2plot)
    triplot = ax.pcolormesh(x, y, data2plot, cmap='viridis', vmin=vmax - 8, vmax=vmax)
    ax.set_aspect('equal')
    ax.set_ylim(z.min() - 0.1, z.max() + 0.1)
    ax.set_xlim(4., r.max() + 0.1)
    ax.xaxis.set_major_locator(loc)
    ax.set_xlabel('R, m', labelpad=0.5)
    ax.set_ylabel('Z, m', labelpad=0)
    fig.subplots_adjust(left=0.13, right=0.75, bottom=0.08, top=0.97)
    pos1 = ax.get_position()
    cax = fig.add_axes([pos1.x0 + pos1.width + 0.04, pos1.y0, 0.04, pos1.height])
    cb = fig.colorbar(triplot, cax=cax)
    cb.set_label(label, labelpad=2.0)

    return fig


if __name__ == "__main__":
    # profcols = {'ne': 4, 'te': 7, 'ti': 8}  # column indices for plasma profiles
    # filename = 'i-ref-0003-1514-01m.ero.divimp_data'
    # data = load_plasma_divimp(filename, firstline=28)  # get plasma data
    profcols = {'Dalpha': 6}  # column indices for plasma profiles
    filename = 'i-ref-0003-90d.mhf.h_alpha'
    data = np.loadtxt(filename, skiprows=14)  # get plasma data
    ln = data[:, 1].astype(int)  # grid line numbers
    r0 = data[:, 2]  # R, m
    z0 = data[:, 3]  # Z, m
    profiles0 = {key: data[:, val] for key, val in profcols.items()}  # plasma profiles (Ne, Te, Ti, Dalpha)
    r, z, profiles = grind_mesh(ln, r0, z0, profiles0, factor=8, kind='linear')  # grind mesh along grid lines
    tri = Delaunay(np.array([r, z]).T)  # create triangulation
    mask = TriMask(r, z, tri.simplices)(feature='perimeter')  # create mask for redundant triangles
    bad_simpl_indx, = np.where(mask)  # get indices for redundant triangles
    r_reg = np.linspace(r0.min(), r0.max(), 256)  # regular mesh for R
    z_reg = np.linspace(z0.min(), z0.max(), 512)  # regular mesh for Z
    r2d, z2d = np.meshgrid(r_reg, z_reg)  # 2D rectangular mesh
    points_new = np.array([r2d.flatten(), z2d.flatten()]).T  # 2D mesh -> points array
    simpl_indx = tri.find_simplex(points_new)  # find indices of triangles for each point
    mask_data = np.in1d(simpl_indx, bad_simpl_indx)  # find indices of the points that are in redundant triangles
    profiles2d = {}
    for key, val in profiles.items():
        prof = LinearNDInterpolator(tri, profiles[key], fill_value=np.nan)(points_new)  # linear interpolation
        # prof = CloughTocher2DInterpolator(tri, profiles['ne'], fill_value=0)(points_new)  # cubic interpolation (use with caution!)
        prof[mask_data] = np.nan  # fill points in redundant triangles with NaNs
        profiles2d[key] = prof.reshape(r2d.shape)  # reshape to 2D
    labels = {'ne': '$N_e$, log$_{10}$(m$^{-3}$)', 'te': '$T_e$, log$_{10}$(eV)', 'ti': '$T_i$, log$_{10}$(eV)',
              'Dalpha': r'D$\alpha$, log$_{10}$(W/m$^3$)'}
    key = 'Dalpha'  # what to plot
    fig_tri = plot_tri(r, z, tri.simplices, profiles[key], mask=mask,
                       label=labels[key], original_mesh=(ln, r0, z0))  # triangular grid pseudocolor (logarithmic)
    fig_reg = plot_reg(r2d, z2d, profiles2d[key], label=labels[key])  # regular grid pseudocolor (logarithmic)
    plt.show()
