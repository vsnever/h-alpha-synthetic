# -*- coding: utf-8 -*-
# In this example the radiance from multiple spectral lines is calculated simultaniously, which significantly reduces the computational time
# compared to the case when the radiance for each spectral line is calculated one by one.
# Note that adaptive sampling can be used effectively only if the emission profiles are not very different for different spectral lines.
# Therefore it is not recomended to mix the emissions from impurities and hydrogen isotopes in a single Raysect simulation.  
from __future__ import print_function
import numpy as np
from raysect.optical import World, translate, rotate
from raysect.optical.material import AbsorbingSurface
from raysect.optical.observer import PinholeCamera, SpectralRadiancePipeline2D
from raysect.primitive import Cylinder, Subtract
from raysect.core.workflow import MulticoreEngine
from synthdiag import read_divimp_reg, divimp_fit_to_3d_wall, load_diag_data
from synthdiag.raysect import SpectralAdaptiveSampler2D, RegularRPhiZIntegrator, DiscreteSpectralRPhiZEmitter, load_pfc_mesh, FovCamera
from matplotlib import pyplot as plt


if __name__ == "__main__":
    case = '2226'
    diag = '55.E2'
    view = 'EP11_top'
    reflections = True
    detailed_wall = False
    dphi = dtheta = 0.05
    step = 0.0025
    cutoff = 0.1 if reflections else 0.05
    cutoff_frac = 0.01 if detailed_wall else 0.001
    min_samples = 1000 if reflections else 100
    nr = 460
    nz = 945
    rmin = 3.95
    rmax = 8.55
    zmin = -4.65
    zmax = 4.8
    wlmin = 655.6
    wlmax = 656.6
    eps_rz = 1.e-4
    roughness = {'Be': 0.26, 'W': 0.29, 'Ss': 0.13}
    view_param = load_diag_data()[diag][view]
    r, dr = np.linspace(rmin, rmax, nr + 1, retstep=True)
    z, dz = np.linspace(zmin, zmax, nz + 1, retstep=True)
    divimp_filename = 'lines_%s.txt' % case
    with open(divimp_filename, 'r') as f:
        keys_all = f.readlines()[2].split()[4:]
    profcols = {key: i + 4 for i, key in enumerate(keys_all)}
    divimpprof = read_divimp_reg(divimp_filename, r, z, profcols=profcols, skiprows=3)
    keys = [key for key in keys_all if divimpprof[key].max() > 0]  # removing all zero-only profiles
    # Note that all deuterium (or all non-deuterium) spectral lines also can be removed if needed
    # keys = [key for key in keys if key[0] != 'D']
    # keys = [key for key in keys if key[0] == 'D']
    lines_wavelength = np.array([0.1 * float(key[2:]) for key in keys])
    min_wavelength = lines_wavelength.min()
    max_wavelength = lines_wavelength.max() + 0.1
    delta_wavelength = np.diff(np.sort(lines_wavelength)).min()
    bins = int((max_wavelength - min_wavelength) / delta_wavelength) + 1
    emiss = np.zeros((nr, nz, len(keys)))
    for i, key in enumerate(keys):
        emiss[:, :, i] = divimpprof[key]
    world = World()
    central_column = Cylinder(rmin, zmax - zmin, material=AbsorbingSurface(), parent=world, transform=translate(0, 0, zmin))
    mesh = load_pfc_mesh(world, reflections=reflections, roughness=roughness, detailed=detailed_wall)
    emitt_material = DiscreteSpectralRPhiZEmitter(emiss, lines_wavelength, min_wavelength, max_wavelength, bins,
                                                  rmin, dr, dz, integrator=RegularRPhiZIntegrator(step))
    print(emitt_material.indx)
    emitter = Subtract(Cylinder(rmax - eps_rz, zmax - zmin - eps_rz), Cylinder(rmin + eps_rz, zmax - zmin - eps_rz),
                       material=emitt_material, parent=world, transform=translate(0, 0, zmin))
    pipeline = SpectralRadiancePipeline2D()
    sampler = SpectralAdaptiveSampler2D(pipeline, fraction=1., cutoff=cutoff, min_samples=min_samples, ratio=10000.)
    if view == 'pinhole':
        camera = PinholeCamera((512, 512), fov=90., parent=world, transform=translate(8.3118, 1.4656, 0) * rotate(90., -10., -90.),
                               pipelines=[pipeline], frame_sampler=sampler)
    else:
        pixels = (int(round(view_param['fov'][0] / dphi)), int(round(view_param['fov'][1] / dtheta)))
        camera = FovCamera(pixels, fov=view_param['fov'], parent=world, pipelines=[pipeline], frame_sampler=sampler,
                           transform=translate(*view_param['pupil']) * rotate(*view_param['rotation']))
    camera.min_wavelength = min_wavelength
    camera.max_wavelength = max_wavelength
    camera.spectral_bins = bins
    camera.spectral_rays = 1
    camera.pixel_samples = min_samples
    camera.ray_extinction_prob = 0
    camera.render_engine = MulticoreEngine(24)
    imsize = camera.pixels[0] * camera.pixels[1]
    npix = imsize
    # start ray tracing
    while npix > cutoff_frac * imsize:
        camera.observe()
        errors = pipeline.frame.errors()[:, :, emitt_material.indx]
        mean = pipeline.frame.mean[:, :, emitt_material.indx]
        ind = np.where((mean > 1.e-4 * mean.max((0, 1))[None, None, :]).sum(2))
        rel_err = (errors[ind] / mean[ind]).max(1)
        npix = np.sum(rel_err > cutoff)
        print("Max. error: %.2e. Pixels to add samples for: %d" % (rel_err.max(), npix))
        for i, key in enumerate(keys):
            fname = "%s_%s_%s_case_%s" % (key, diag, view, case)
            np.savetxt(fname + '.txt', mean[:, :, i])
            np.savetxt(fname + '_errors.txt', errors[:, :, i])
