# -*- coding: utf-8 -*-
from __future__ import print_function
import numpy as np
from raysect.optical import World, translate, rotate
from raysect.optical.material import AbsorbingSurface
from raysect.optical.observer import FullFrameSampler2D
from raysect.core.workflow import MulticoreEngine
from raysect.primitive import Cylinder, Subtract
from synthdiag import load_diag_data
from synthdiag.raysect import FovCamera
from synthdiag.raysect.rtm import RtmPipeline2D, RtmRegularRPhiZEmitter, RtmRegularRPhiZIntegrator, load_pfc_mesh


def calc_partial_rtm(pixels, view_param, pixel_samples, r, z, iz, iz_end, step, roughness, reflections, detailed_wall):
    eps_rz = 1.e-4
    nr = r.size - 1
    dr = r[1] - r[0]
    dz = z[1] - z[0]
    nzi = iz_end - iz
    world = World()
    central_column = Cylinder(r[0], z[-1] - z[0], material=AbsorbingSurface(), parent=world, transform=translate(0, 0, z[0]))
    mesh = load_pfc_mesh(world, reflections=reflections, roughness=roughness, detailed=detailed_wall)
    emitt_material = RtmRegularRPhiZEmitter(nr, nzi, dr, dz, r[0], integrator=RtmRegularRPhiZIntegrator(step))
    emitter = Subtract(Cylinder(r[-1] - eps_rz, z[iz_end] - z[iz] - eps_rz), Cylinder(r[0] + eps_rz, z[iz_end] - z[iz] - eps_rz),
                       material=emitt_material, parent=world, transform=translate(0, 0, z[iz]))
    pipeline = RtmPipeline2D()
    sampler = FullFrameSampler2D()
    camera = FovCamera(pixels, fov=view_param['fov'], parent=world, pipelines=[pipeline], frame_sampler=sampler,
                       transform=translate(*view_param['pupil']) * rotate(*view_param['rotation']))
    camera.min_wavelength = 656.0
    camera.max_wavelength = 656.2
    camera.spectral_bins = nr * nzi
    camera.spectral_rays = 1
    camera.pixel_samples = pixel_samples
    camera.ray_extinction_prob = 0
    camera.ray_importance_sampling = False
    camera.render_engine = MulticoreEngine(12)
    camera.observe()

    return pipeline.rtm


if __name__ == "__main__":
    diag = '55.E2'
    view = 'EP11_top'
    reflections = True
    detailed_wall = False
    dtheta = 0.1  # degree
    dphi = 0.6  # degree
    step = 0.001  # m
    pixel_samples = 60000 if reflections else 600
    nr = 460
    nz = 945
    roughness = {'Be': 0.26, 'W': 0.29, 'Ss': 0.13}
    view_param = load_diag_data()[diag][view]
    view_param['fov'] = [dphi, view_param['fov'][1]]  # cut a narrow vertical strip from the FoV
    r, dr = np.linspace(3.95, 8.55, nr + 1, retstep=True)
    z, dz = np.linspace(-4.65, 4.8, nz + 1, retstep=True)
    izstep = 95
    npix_x = 1  # just one pixel (0.6deg-wide)
    npix_y = int(round(view_param['fov'][1] / dtheta))
    rtm = np.zeros((npix_x, npix_y, nr, nz), dtype=np.float32)
    for iz in range(0, nz, izstep):
        iz_end = min(iz + izstep, nz)
        partial_rtm = calc_partial_rtm((npix_x, npix_y), view_param, pixel_samples,
                                       r, z, iz, iz_end, step, roughness, reflections, detailed_wall)
        rtm[:, :, :, iz:iz_end] = partial_rtm.astype(np.float32).reshape((npix_x, npix_y, nr, iz_end - iz))
    np.save('RTM_%s.npy' % view, rtm)
